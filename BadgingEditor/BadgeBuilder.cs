﻿//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//#if NETCOREAPP
//using CommonCore.Badging;
//#endif

//namespace BadgingEditor
//{

//#if NETCOREAPP

//    public class BadgeBuilder
//    {
//        public static BadgeBuilder Default { get; } = new BadgeBuilder();

//        public virtual string Build(Badge badge, bool includeXmlDeclaration = true)
//        {
//            //int badgeRoundness = 5;
//            Size badgeSize = this.CalBadgeSize(badge);

//            var builder = new StringBuilder();

//            if (includeXmlDeclaration) { builder.AppendLine(@"<?xml version=""1.0"" encoding=""UTF-8""?>"); }
//            builder.AppendLine($@"<svg width=""{badgeSize.Width}"" height=""{badgeSize.Height}"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">");

//            if (badge.IsNotEmpty())
//            {
//                // Gather all the non-empty sections.
//                IReadOnlyList<BadgeSection> sections = badge.Sections.Where(section => section.IsNotEmpty()).ToArray();
//                // Define gloss gradient, but only if needed by non-empty sections.
//                if (sections.Any(section => section.EnableColorGloss))
//                {
//                    builder.AppendLine(@"    <defs>");
//                    builder.AppendLine(@"        <linearGradient id=""gloss"" x2=""0"" y2=""100%"">");
//                    builder.AppendLine(@"            <stop offset=""0"" stop-color=""#bbbbbb"" stop-opacity="".1""/>");
//                    builder.AppendLine(@"            <stop offset=""1"" stop-opacity="".1""/>");
//                    builder.AppendLine(@"        </linearGradient>");
//                    builder.AppendLine(@"    </defs>");
//                }

//                var point = Point.Empty;
//                for (int index = 0; index < sections.Count; index++)
//                {
//                    var section = sections[index];
//                    bool first = index == 0;
//                    bool last = index == sections.Count - 1;
//                    Size sectionSize = this.CalSectionSize(badge, section);
//                    // Draw the section background.
//                    this.AppRoundedRect(builder, point, sectionSize, badge.GetRoundness(), first, last, section.Color.ToHex());
//                    // Apply gloss to the section background.
//                    if (section.EnableColorGloss)
//                    { this.AppRoundedRect(builder, point, sectionSize, badge.GetRoundness(), first, last, "url(#gloss)"); }

//                    if (section.IconExists)
//                    {
//                        var iconRect = this.CalIconRect(badge, section);
//                        builder.AppendLine($@"    <image x=""{point.X + iconRect.X}"" y=""{point.Y + iconRect.Y}"" width=""{iconRect.Width}"" height=""{iconRect.Height}"" viewBox=""0 0 {iconRect.Width} {iconRect.Height}"" xlink:href=""data:image/svg+xml;base64,{section.Icon}"" />");

//                        point.Offset(this.CalIconPaddedWidth(badge, section), 0);
//                    }

//                    if (section.TextExists)
//                    {
//                        var textRect = this.CalTextRect(badge, section);
//                        // width=""{textArea.Width}"" height=""{textArea.Height}""
//                        // text-anchor=""middle"" dominant-baseline=""middle"" alignment-baseline=""middle""
//                        builder.AppendLine($@"    <g font-family=""{section.Font.Name}"" font-size=""{section.Font.Size}"">");
//                        if (section.EnableTextShadow)
//                        { builder.AppendLine($@"        <text x=""{point.X + textRect.X + 1}"" y=""{point.Y + textRect.Y + 1}"" fill=""#010101"" fill-opacity="".3"">{section.Text}</text>"); }
//                        builder.AppendLine($@"        <text x=""{point.X + textRect.X}"" y=""{point.Y + textRect.Y}"" fill=""{section.Font.Color.ToHex()}"">{section.Text}</text>");
//                        builder.AppendLine(@"    </g>");

//                        point.Offset(this.CalTextPaddedWidth(badge, section), 0);
//                    }
//                }
//            }

//            builder.AppendLine(@"</svg>");
//            return builder.Replace("\r\n", "\n").ToString();
//        }

//        public virtual int CalBadgeWidth(Badge badge)
//            => badge.Sections.Sum(section => this.CalSectionWidth(badge, section));

//        public virtual int CalSectionWidth(Badge badge, BadgeSection section)
//            => section.IsEmpty() ? 0 : this.CalIconPaddedWidth(badge, section) + this.CalTextPaddedWidth(badge, section);

//        public virtual int CalIconPaddedWidth(Badge badge, BadgeSection section)
//            => !section.IconExists ? 0 : ((int)section.IconPadding * 2) + this.CalIconSize(badge, section).Width;

//        public virtual int CalTextPaddedWidth(Badge badge, BadgeSection section)
//            => !section.TextExists ? 0 : ((int)section.TextPadding * 2) + (int)Math.Ceiling(section.Font.CalTextSize(section.Text).Width);

//        public virtual Size CalBadgeSize(Badge badge)
//            => new Size(this.CalBadgeWidth(badge), badge.GetHeight());

//        public virtual Size CalSectionSize(Badge badge, BadgeSection section)
//            => new Size(this.CalSectionWidth(badge, section), badge.GetHeight());

//        // Ideally we would get the embedded size of the icon and scale the
//        // width down based on the padded shrinking of the height, allowing
//        // the section width to expand for horizontally longer rectangles.
//        // However currently we can't get the embedded size, so assume the
//        // icon width is equal to the badge height and shrink both equally.
//        public virtual Size CalIconSize(Badge badge, BadgeSection section)
//            => !section.IconExists ? Size.Empty : new Size(badge.GetHeight() - ((int)section.IconPadding * 2), badge.GetHeight() - ((int)section.IconPadding * 2));

//        public virtual Size CalTextSize(Badge badge, BadgeSection section)
//            => !section.TextExists ? Size.Empty : this.Ceiling(section.Font.CalTextSize(section.Text));

//        public virtual Rectangle CalIconRect(Badge badge, BadgeSection section)
//        {
//            if (!section.IconExists) { return Rectangle.Empty; }
//            var size = this.CalIconSize(badge, section);
//            var point = new Point((int)section.IconPadding, (int)section.IconPadding);
//            return new Rectangle(point, size);
//        }

//        public virtual Rectangle CalTextRect(Badge badge, BadgeSection section)
//        {
//            if (!section.TextExists) { return Rectangle.Empty; }
//            var size = this.CalTextSize(badge, section);
//            var point = new Point((int)section.TextPadding, size.Height + 1);
//            return new Rectangle(point, size);
//        }

//        protected Size Ceiling(SizeF size)
//        {
//            float width = size.Width;
//            float height = size.Height;
//            return new Size((width > (int)width ? (int)width + 1 : (int)width), (height > (int)height ? (int)height + 1 : (int)height));
//        }

//        protected virtual void AppRoundedRect(StringBuilder builder, Point point, Size size, int roundness, bool left, bool right, string fill)
//        {
//            if (roundness > 0 && left && right)
//            {
//                Size small = size - new Size(roundness * 2, roundness * 2);
//                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X + roundness},{point.Y} h{small.Width} q{roundness},0 {roundness},{roundness} v{small.Height} q0,{roundness} -{roundness},{roundness} h-{small.Width} q-{roundness},0 -{roundness},-{roundness} v-{small.Height} q0,-{roundness} {roundness},-{roundness} z"" /> ");
//            }
//            else if (roundness > 0 && left)
//            {
//                Size small = size - new Size(roundness, roundness * 2);
//                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X + size.Width},{point.Y} h-{small.Width} q-{roundness},0 -{roundness},{roundness} v{small.Height} q0,{roundness} {roundness},{roundness} h{small.Width} z"" /> ");
//            }
//            else if (roundness > 0 && right)
//            {
//                Size small = size - new Size(roundness, roundness * 2);
//                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X},{point.Y} h{small.Width} q{roundness},0 {roundness},{roundness} v{small.Height} q0,{roundness} -{roundness},{roundness} h-{small.Width} z"" /> ");
//            }
//            else
//            {
//                builder.AppendLine($@"    <path fill=""{fill}"" d=""M{point.X},{point.Y} h{size.Width} v{size.Height} h-{size.Width} z"" /> ");
//            }
//        }
//    }
//#endif
//}
