﻿using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
#if NETCOREAPP
using CommonCore.Badging;
using Newtonsoft.Json;
using Svg;
#endif

namespace BadgingEditor
{
    public static class BadgeExtensions
    {
#if NETCOREAPP

        internal static int GetHeight(this Badge self) => 20;

        internal static int GetRoundness(this Badge self) => 5;

        public static Bitmap ToBitmap(this Badge self)
            => SvgDocument.FromSvg<SvgDocument>(self.ToTestSvg()).Draw();

        public static string ToJson(this Badge self, Formatting formatting = Formatting.Indented)
            => JsonConvert.SerializeObject(self, formatting);

        public static string ToTestSvg(this Badge self) => self.ToTestSvg(true);

        public static string ToTestSvg(this Badge self, bool includeXmlDeclaration)
        {
            return self.ToSvg(includeXmlDeclaration);
        }

#endif
    }
}
