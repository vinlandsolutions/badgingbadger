﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Common.WinForms.Controls.PrivateExtensions;

// https://www.codeproject.com/Articles/21085/CheckBox-ComboBox-Extending-the-ComboBox-Class-and
namespace Common.WinForms.Controls
{
    /// <summary>
    /// The <see cref="ObjectComboBox"/> class is a more advanced version of the standard
    /// <see cref="ComboBox"/> control and allows greater control over how the contained
    /// objects are displayed.
    /// </summary>
    /// <remarks>
    /// ToDo: This control needs to be rewritten as a new control named <c>ObjectComboControl</c>.
    /// The primary goal of rewriting is to use a more consistent and comprehensible naming scheme
    /// for the aspect-related members.
    /// <list type="bullet">
    ///     <listheader>
    ///         <term>Property</term>
    ///         <description>Purpose</description>
    ///     </listheader>
    ///     <item>
    ///         <term>TextAspect</term>
    ///         <description>A string representing the property to use for the text value.</description>
    ///     </item>
    ///     <item>
    ///         <term>TextGetter</term>
    ///         <description>A delegate that returns the text value.</description>
    ///     </item>
    /// </list>
    /// </remarks>
    [DefaultEvent("SelectedIndexChanged"), DefaultProperty("PlaceholderText")]
    public class ObjectComboBox : ComboBox
    {
        public const string CATEGORY = "ObjectComboBox";

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectComboBox"/>  class.
        /// </summary>
        public ObjectComboBox()
        {
            base.FormattingEnabled = this.FormattingEnabled;
            base.DropDownStyle = this.DropDownStyle;
            base.DrawMode = this.DrawMode;
        }

        #region Replaced Base Methods

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new DrawMode DrawMode { get => DrawMode.OwnerDrawFixed; set { } }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new ComboBoxStyle DropDownStyle { get => ComboBoxStyle.DropDownList; set { } }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool FormattingEnabled { get => true; set { } }

        #endregion

        [DefaultValue(null), Category(CATEGORY), Description("Image list used to display item images.")]
        public ImageList ImageList { get; set; } = null;

        [DefaultValue(false), Category(CATEGORY), Description("Toggle lines between each item in the drop down list.")]
        public bool GridLines
        {
            get => this._gridLines;
            set => this.SetProperty(ref this._gridLines, ref value);
        }
        protected bool _gridLines = false;

        [DefaultValue(typeof(Color), "DimGray"), Category(CATEGORY), Description("The color grid lines displayed between each item in the drop down list.")]
        public Color GridLineColor
        {
            get => this._gridLineColor;
            set => this.SetProperty(ref this._gridLineColor, ref value);
        }
        protected Color _gridLineColor = Color.DimGray;

        [DefaultValue(null), Category(CATEGORY), Description("The text displayed when no item is selected.")]
        public string PlaceholderText
        {
            get => this._placeholderText;
            set => this.SetProperty(ref this._placeholderText, value);
        }
        protected string _placeholderText = null;

        [DefaultValue(typeof(Color), "DimGray"), Category(CATEGORY), Description("The color of the text displayed when no item is selected.")]
        public Color PlaceholderColor
        {
            get => this._placeholderColor;
            set => this.SetProperty(ref this._placeholderColor, ref value);
        }
        protected Color _placeholderColor = Color.DimGray;

        [DefaultValue(null), DisplayName("TextAspect"), Category(CATEGORY), Description("")]
        public string TextAspectName { get; set; } = null;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<object, string> TextAspectGetter { get; set; } = null;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<object, string> TextGetter { get; set; } = null;

        [DefaultValue(null), DisplayName("ImageKeyAspect"), Category(CATEGORY), Description("")]
        public string ImageKeyAspectName { get; set; } = null;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<object, string> ImageKeyAspectGetter { get; set; } = null;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<object, string> ImageKeyGetter { get; set; } = null;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<object, Image> ImageGetter { get; set; } = null;

        [DefaultValue(null), DisplayName("ColorAspect"), Category(CATEGORY), Description("")]
        public string ColorAspectName { get; set; } = null;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<object, string> ColorAspectGetter { get; set; } = null;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<object, Color?> ColorGetter { get; set; } = null;

        public void ClearObjects() => this.Items.Clear();

        public void AddObject(object model) => this.Items.Add(model);

        public void AddObjects(IEnumerable models) { foreach (var model in models) { this.AddObject(model); } }

        public void SetObjects(IEnumerable models)
        {
            this.ClearObjects();
            this.AddObjects(models);
        }

        protected override void OnFormat(ListControlConvertEventArgs e) => base.OnFormat(e);

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            e.DrawBackground();
            if (e.Index > -1)
            {
                this.DrawItemImage(e);
                this.DrawItemText(e);
                if (this.DroppedDown && this.GridLines && e.Index > 0)
                {
                    using (var pen = new Pen(this.GridLineColor))
                    {
                        e.Graphics.DrawLine(pen,
                            new Point(e.Bounds.Left + 2, e.Bounds.Top),
                            new Point(e.Bounds.Right - 2, e.Bounds.Top));
                    }
                }
            }
            else if (!this.PlaceholderText.IsEmpty())
            {
                this.DrawItemText(e, this.PlaceholderText, this.PlaceholderColor, false);
            }
            base.OnDrawItem(e);
        }

        protected void DrawItemImage(DrawItemEventArgs e)
        {
            //if (!this.HasImages()) { return; }

            object obj = base.Items[e.Index];
            if (obj == null) { return; }
            var image = this.GetImage(obj);
            if (image == null) { return; }

            var rect = this.GetImageRect(e.Bounds);
            e.Graphics.DrawImage(image, rect);
            //e.Graphics.DrawRectangle(Pens.Black, rect);
        }

        protected void DrawItemText(DrawItemEventArgs e)
        {
            object obj = base.Items[e.Index];
            if (obj == null) { return; }
            string text = this.GetText(obj);
            if (text.IsEmpty()) { return; }
            var convertEventArgs = new ListControlConvertEventArgs(text, typeof(string), obj);
            this.OnFormat(convertEventArgs);
            this.DrawItemText(e, convertEventArgs.Value.ToString(), this.GetColor(obj) ?? e.ForeColor, this.HasImages());
        }

        protected void DrawItemText(DrawItemEventArgs e, string text, Color color, bool hasImage)
        {
            if (text.IsEmpty()) { return; }
            using (SolidBrush brush = new SolidBrush(color))
            {
                ////Size size = (!hasImage ? Size.Empty : this.GetImageSize());
                //var rect = (!hasImage ? Rectangle.Empty : this.GetImageRect(e.Bounds));
                //e.Graphics.DrawString(text, e.Font,
                //brush, (float)(rect.Right + 2), (float)(e.Bounds.Y + 1));
                e.Graphics.DrawString(text, e.Font, brush, this.GetTextRect(e.Bounds, hasImage));
            }
        }

        protected bool HasImages() => this.ImageGetter != null || (this.ImageList != null && this.ImageList.Images.Count > 0);

        protected Size GetImageSize()
        {
            int size = (int)Math.Floor((double)this.ItemHeight * 0.75);
            return new Size(size, size);
        }

        protected Rectangle GetImageRect(Rectangle bounds)
        {
            Size size = this.GetImageSize();
            int x = bounds.X + 2;
            //int y = e.Bounds.Y + (int)Math.Floor((double)(e.Bounds.Height - num) / 2.0);
            int y = bounds.Y + (int)Math.Floor((double)(bounds.Height - size.Height) / 2.0);
            return new Rectangle(x, y, size.Width, size.Height);
        }

        protected Rectangle GetTextRect(Rectangle bounds, bool hasImage)
        {
            var rect = (!hasImage ? Rectangle.Empty : this.GetImageRect(bounds));
            return new Rectangle(rect.Right + 2, bounds.Top + 1, bounds.Width - rect.Right - 2 - 2, bounds.Height - 1);
        }

        /// <summary>
        /// if aspect getter is null or returns null ->
        /// if aspect name is empty then null else value.
        /// </summary>
        protected string GetImageKeyAspect(object obj)
            => this.ImageKeyAspectGetter?.Invoke(obj) ?? (this.ImageKeyAspectName.IsEmpty() ? null : this.ImageKeyAspectName);

        /// <summary>
        /// if image key getter is null or returns null ->
        /// if aspect value is null ->
        /// then return null.
        /// </summary>
        protected string GetImageKey(object obj)
            => this.ImageKeyGetter?.Invoke(obj) ?? obj.GetAspectValue(this.GetImageKeyAspect(obj))?.ToString();

        protected Image GetImage(object obj)
        {
            if (!this.HasImages()) { return null; }
            if (this.ImageGetter != null) { return this.ImageGetter(obj); }

            var key = this.GetImageKey(obj);
            if (key == null || !this.ImageList.Images.ContainsKey(key)) { return null; }
            return this.ImageList.Images[key];
        }

        /// <summary>
        /// if aspect getter is null or returns null ->
        /// if aspect name is empty then null else value.
        /// </summary>
        protected string GetTextAspect(object obj)
            => this.TextAspectGetter?.Invoke(obj) ?? (this.TextAspectName.IsEmpty() ? null : this.TextAspectName);

        /// <summary>
        /// if text getter is null or returns null ->
        /// if aspect value is null ->
        /// then return object to string.
        /// </summary>
        protected string GetText(object obj)
            => this.TextGetter?.Invoke(obj) ?? obj.GetAspectValue(this.GetTextAspect(obj))?.ToString() ?? obj.ToString();

        /// <summary>
        /// if aspect getter is null or returns null ->
        /// if aspect name is empty then null else value.
        /// </summary>
        protected string GetColorAspect(object obj)
            => this.ColorAspectGetter?.Invoke(obj) ?? (this.ColorAspectName.IsEmpty() ? null : this.ColorAspectName);

        /// <summary>
        /// if text getter is null or returns null ->
        /// if aspect value is null ->
        /// then return null.
        /// </summary>
        protected Color? GetColor(object obj)
            => this.ColorGetter?.Invoke(obj) ?? (Color?)obj.GetAspectValue(this.GetColorAspect(obj));

        // Fix drop down width

        private void UpdateDropDownWidth()
        {
            using (var ds = this.CreateGraphics())
            {
                double max = 0;
                foreach (object item in this.Items)
                //{ max = Math.Max(max, TextRenderer.MeasureText(this.GetText(item), this.Font).Width); }
                { max = Math.Max(max, ds.MeasureString(this.GetText(item), this.Font).Width); }
                //{
                //    var text = this.GetText(item);
                //    double w0 = TextRenderer.MeasureText(text, this.Font).Width;
                //    double w1 = ds.MeasureString(text, this.Font).Width;
                //    max = Math.Max(max, w1);
                //}
                if (this.HasImages()) { max += this.GetImageSize().Width + 2; }
                max += 8;

                int width = Math.Min((int)Math.Ceiling(max), Screen.GetWorkingArea(this).Width);
                if (width > this.DropDownWidth) { this.DropDownWidth = width; }
            }
        }

        private const UInt32 WM_CTLCOLORLISTBOX = 0x0134;
        private const int SWP_NOSIZE = 0x1;
        private const int CB_ADDSTRING = 0x0143;
        private const int CB_DELETESTRING = 0x0144;
        private const int CB_INSERTSTRING = 0x014A;
        private const int CB_RESETCONTENT = 0x014B;

        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == CB_ADDSTRING)
            {
                base.WndProc(ref m);
                //int index = (this.Items.Count - 1);
                //this.NotifyAdded(index);
                this.UpdateDropDownWidth();
            }
            else if (m.Msg == CB_DELETESTRING)
            {
                base.WndProc(ref m);
                //int index = m.WParam.ToInt32();
                //this.NotifyRemoved(index);
                this.UpdateDropDownWidth();
            }
            else if (m.Msg == CB_INSERTSTRING)
            {
                base.WndProc(ref m);
                //int index = m.WParam.ToInt32();
                //this.NotifyAdded((index > -1 ? index : this.Items.Count - 1));
                this.UpdateDropDownWidth();
            }
            else if (m.Msg == CB_RESETCONTENT)
            {
                base.WndProc(ref m);
                //this.NotifyCleared();
                this.UpdateDropDownWidth();
            }
            else if (m.Msg == WM_CTLCOLORLISTBOX)
            {
                int left = this.PointToScreen(new Point(0, 0)).X;
                if (this.DropDownWidth > Screen.PrimaryScreen.WorkingArea.Width - left)
                {
                    Rectangle comboRect = this.RectangleToScreen(this.ClientRectangle);

                    int x = 0;
                    int y = 0;
                    int height = 0;

                    for (int i = 0; (i < this.Items.Count && i < this.MaxDropDownItems); i++)
                    {
                        height += this.ItemHeight;
                    }

                    if (height > Screen.PrimaryScreen.WorkingArea.Height - this.PointToScreen(new Point(0, 0)).Y)
                    {
                        y = comboRect.Top - height - 2;
                    }
                    else
                    {
                        y = comboRect.Bottom;
                    }

                    x = comboRect.Left - (this.DropDownWidth - (Screen.PrimaryScreen.WorkingArea.Width - left));
                    SetWindowPos(m.LParam, IntPtr.Zero, x, y, 0, 0, SWP_NOSIZE);
                }
                base.WndProc(ref m);
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        protected bool SetProperty<T>(ref T prop, T value) where T : class
        {
            if (prop == null && value == null) { return false; }
            else if (prop == null || value == null || !prop.Equals(value))
            {
                prop = value;
                this.Invalidate();
                return true;
            }
            else { return false; }
        }

        protected bool SetProperty<T>(ref T prop, ref T value) where T : struct
        {
            if (!prop.Equals(value))
            {
                prop = value;
                this.Invalidate();
                return true;
            }
            else { return false; }
        }
    }

    namespace PrivateExtensions
    {
        internal static class StringExtensions
        {
            public static bool IsEmpty(this string self) => self == null || self.Length == 0;
        }

        internal static class ComboBoxExtensions
        {
            public static object GetAspectValue(this object self, string aspect)
                => (aspect.IsEmpty() ? null : self?.GetType().GetProperty(aspect)?.GetValue(self, null));

            public static T GetAspectValue<T>(this object self, string aspect) where T : class
                => (T)self.GetAspectValue(aspect);
        }
    }
}
