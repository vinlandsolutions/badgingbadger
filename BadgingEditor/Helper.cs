﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Common.WinForms.Controls;
#if NETCOREAPP
using CommonCore.Badging;
using Svg;
#endif

namespace BadgingEditor
{
    public static class Helper
    {
#if NETCOREAPP

        public static void CacheColorImages(Size size)
        {
            foreach (var key in BadgeColors.Dict.Keys)
            { if (key.Length > 0) { CacheColorImage(size, key); } }
        }

        public static void CacheColorImage(Size size, string key)
        {
            var bitmap = new Bitmap(size.Width, size.Height);
            using var graphics = Graphics.FromImage(bitmap);
            using var brush = new SolidBrush(BadgeColors.Dict[key]);
            graphics.FillRectangle(brush, 0, 0, size.Width, size.Height);
            graphics.DrawRectangle(Pens.Black, 0, 0, size.Width - 1, size.Height - 1);
            _colorImageCache.Add(key, bitmap);
        }
        private static readonly Dictionary<string, Image> _colorImageCache = new Dictionary<string, Image>();

        public static void CacheIconImages(Size size)
        {
            var temp = new Dictionary<string, string>();
            temp.Add("none", null);
            foreach (var pair in BadgeIcons.Dict) { temp.Add(pair.Key, pair.Value); }
            BadgeIcons.Dict.Clear();
            foreach (var pair in temp) { BadgeIcons.Dict.Add(pair.Key, pair.Value); }
            foreach (var key in BadgeIcons.Dict.Keys) { if (key.Length > 0) { CacheIconImage(size, key); } }
        }

        public static void CacheIconImage(Size size, string key)
        {
            var icon = BadgeIcons.Dict[key];
            var bitmap = new Bitmap(size.Width, size.Height);
            using var graphics = Graphics.FromImage(bitmap);
            using var brush = new SolidBrush(BadgeColors.Default);
            graphics.FillRectangle(brush, 0, 0, size.Width, size.Height);
            if (icon != null)
            {
                var svg = Encoding.UTF8.GetString(Convert.FromBase64String(icon));
                SvgDocument.FromSvg<SvgDocument>(svg).Draw(bitmap);
            }
            _iconImageCache.Add(key, bitmap);
        }
        private static readonly Dictionary<string, Image> _iconImageCache = new Dictionary<string, Image>();



        public static Control CreateRowLabel(int row, string text)
        {
            var control = new Label();
            control.Anchor = AnchorStyles.Right;
            control.AutoSize = true;
            control.Margin = new Padding(1);
            control.Text = text;
            control.Tag = row;
            return control;
        }

        public static Control CreateRemoveButton(int row, EventHandler handler)
        {
            var control = new Button();
            control.Anchor = AnchorStyles.None;
            control.FlatAppearance.BorderSize = 0;
            control.FlatStyle = FlatStyle.Flat;
            control.Margin = new Padding(1);
            control.Size = new System.Drawing.Size(20, 20);
            control.UseVisualStyleBackColor = true;
            control.BackgroundImageLayout = ImageLayout.Zoom;
            control.BackgroundImage = Image.FromFile("./Assets/Graphics/remove.png");
            control.Tag = row;
            control.Click += handler;
            return control;
        }

        public static Control CreateIconCombo(int row, EventHandler handler, BadgeIcon icon)
        {
            var control = new ObjectComboBox();
            control.Dock = DockStyle.Fill;
            control.Margin = new Padding(1);
            control.Size = new Size(100, 21);
            control.Tag = row;

            control.TextGetter = obj => ((KeyValuePair<string, string>)obj).Key;
            control.ImageGetter = obj => _iconImageCache[((KeyValuePair<string, string>)obj).Key];
            control.SetObjects(BadgeIcons.Dict.Where(pair => pair.Key.Length > 0));
            control.SelectedIndex = 0;

            if (!string.IsNullOrEmpty(icon.Content))
            { control.SelectedItem = BadgeIcons.Dict.First(obj => ((KeyValuePair<string, string>)obj).Value == icon.Content); }
            control.SelectedIndexChanged += handler;
            return control;
        }

        public static Control CreateColorCombo(int row, EventHandler handler, Color color)
        {
            var control = new ObjectComboBox();
            control.Dock = DockStyle.Fill;
            control.Margin = new Padding(1);
            control.Size = new Size(100, 21);
            control.Tag = row;

            control.TextGetter = obj => ((KeyValuePair<string, Color>)obj).Key;
            control.ImageGetter = obj => _colorImageCache[((KeyValuePair<string, Color>)obj).Key];
            control.SetObjects(BadgeColors.Dict.Where(pair => pair.Key.Length > 0));
            control.SelectedIndex = 0;

            control.SelectedItem = BadgeColors.Dict.First(obj => ((KeyValuePair<string, Color>)obj).Value == color);
            control.SelectedIndexChanged += handler;
            return control;
        }

        public static Control CreateContentText(int row, EventHandler handler, string text)
        {
            var control = new TextBox();
            control.Dock = DockStyle.Fill;
            control.Margin = new Padding(1);
            control.Size = new Size(100, 21);
            control.Tag = row;

            control.Text = text;
            control.TextChanged += handler;
            return control;
        }

        public static Control CreatePaddingSpin(int row, EventHandler handler)
        {
            var control = new NumericUpDown();
            control.BorderStyle = BorderStyle.FixedSingle;
            control.Margin = new Padding(1);
            control.Maximum = 999;
            control.Minimum = 0;
            control.Size = new Size(50, 21);
            control.Tag = row;
            control.TextAlign = HorizontalAlignment.Right;
            control.Value = 4;

            control.ValueChanged += handler;
            return control;
        }

        //public static BadgeSection CreateSection(ObjectComboBox iconControl, ObjectComboBox colorControl, TextBox contentControl)
        //    => new BadgeSection(GetContent(contentControl), GetColor(colorControl), GetIcon(iconControl));

        public static string GetContent(TextBox text) => text.Text ?? string.Empty;

        public static Color GetColor(ObjectComboBox combo)
            => combo.SelectedItem == null ? BadgeColors.Default : ((KeyValuePair<string, Color>)combo.SelectedItem).Value;

        public static string GetIcon(ObjectComboBox combo)
            => combo.SelectedItem == null ? null : ((KeyValuePair<string, string>)combo.SelectedItem).Value;

        public static int GetPadding(NumericUpDown control)
            => (int)control.Value;

#endif
    }
}
