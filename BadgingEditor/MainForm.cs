﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Common.WinForms.Controls;
#if NETCOREAPP
using CommonCore.Badging;
using Newtonsoft.Json;
using Svg;
#endif

namespace BadgingEditor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            this.InitializeComponent();

#if NETCOREAPP

            //this._layout.Width = this._layout.Parent.ClientRectangle.Width;
            this._layout.RowStyles.Clear();
            this._layout.RowCount = 0;

            this.Icon = Icon.FromHandle(((Bitmap)Bitmap.FromFile("./Assets/Graphics/app.png")).GetHicon());

            this._toolOpen.Image = Bitmap.FromFile("./Assets/Graphics/import.png");
            this._toolOpenJson.Image = Bitmap.FromFile("./Assets/Graphics/type_json.png");

            this._toolSave.Image = Bitmap.FromFile("./Assets/Graphics/export.png");
            this._toolSaveJson.Image = Bitmap.FromFile("./Assets/Graphics/type_json.png");
            this._toolSaveSvg.Image = Bitmap.FromFile("./Assets/Graphics/type_svg.png");
            this._toolSavePng.Image = Bitmap.FromFile("./Assets/Graphics/type_png.png");

            this._toolSectionAdd.Image = Bitmap.FromFile("./Assets/Graphics/add.png");
            this._toolSectionRemove.Image = Bitmap.FromFile("./Assets/Graphics/remove.png");

            this._toolSettings.Image = Bitmap.FromFile("./Assets/Graphics/settings.png");

            //var badge = new Badge(
            //    new BadgeSection(null, BadgeColors.Default, BadgeIcons.Windows)
            //    , new BadgeSection("something", Color.Green)
            //    , new BadgeSection("this is bad", Color.Red, BadgeIcons.Info)
            //    , new BadgeSection("maybe", Color.Orange)
            //    );
            //this.DrawBadge(badge);

            //this._tabs.SelectedIndex = 3;

            //var result = Tests.BadgeIconsGenerator.Run(@"D:\Code\Vinland Solutions\CommonCoreLibs\CommonCore.Badging\Icons");
            //this._text.Clear();
            //this._text.AppendText(result.Builder.ToString());
            ////this._text.AppendText(Environment.NewLine);
            ////this._text.AppendText($"Examples: {result.Count}");

            //string root = "./Assets/infos";
            //Directory.CreateDirectory(root);

            //Directory.CreateDirectory($"{root}/fonts");
            //foreach (var (key, font) in BadgeFonts.Dict)
            //{
            //    var badge = new Badge(BadgeIcons.Info, "font", key.Length == 0 ? "<empty>" : key, BadgeStyle.CreateDefault(BadgeColors.BBlue));
            //    badge.Sections[1].Font = font;
            //    File.WriteAllText($"{root}/fonts/{(key.Length == 0 ? "empty" : key)}.svg", badge.ToTestSvg());
            //}

            //Directory.CreateDirectory($"{root}/icons");
            //foreach (var (key, image) in BadgeIcons.Dict)
            //{
            //    var badge = new Badge(image, "icon", key.Length == 0 ? "<empty>" : key, BadgeStyle.CreateDefault(BadgeColors.BBlue));
            //    File.WriteAllText($"{root}/icons/{(key.Length == 0 ? "empty" : key)}.svg", badge.ToTestSvg());
            //}

            //Directory.CreateDirectory($"{root}/colors");
            //foreach (var (key, color) in BadgeColors.Dict)
            //{
            //    var badge = new Badge(BadgeIcons.Palette, "color", key.Length == 0 ? "<empty>" : key, BadgeStyle.CreateDefault(color));
            //    File.WriteAllText($"{root}/colors/{(key.Length == 0 ? "empty" : key)}.svg", badge.ToTestSvg());
            //}


            //var builder = new StringBuilder();
            //string format;
            //int index;

            //foreach (var (key, font) in BadgeFonts.Dict)
            //{
            //    if (key.Length == 0) { continue; }
            //    var badge = new Badge(BadgeIcons.Info, "font", key.Length == 0 ? "<empty>" : key, BadgeStyle.CreateDefault(BadgeColors.BBlue));
            //    badge.Sections[1].Font = font;
            //}

            //index = 0;
            //format = "| {0,5} | {1,20} | {2,-84} |";
            //builder.Clear().AppendLine().AppendLine($"Count: {BadgeFonts.Dict.Count}").AppendLine();
            //builder.AppendFormat(format, "Index", "Title", "Example").AppendLine();
            //builder.AppendLine($"|{new string('-', 6)}:|{new string('-', 21)}:|:{new string('-', 85)}|");
            //foreach (var (key, font) in BadgeFonts.Dict)
            //{
            //    var name = key.Length == 0 ? "empty" : key;
            //    builder.AppendFormat(format,
            //        index++, key, $"![{name} Font Badge](../images/infos/fonts/{name}.svg)").AppendLine();
            //}
            //this._text.AppendText(builder.ToString());

            //index = 0;
            //format = "| {0,5} | {1,20} | {2,12} | {3,-84} |";
            //int totalSize = BadgeIcons.Dict.Sum(pair => Encoding.UTF8.GetByteCount(pair.Value));
            //builder.Clear().AppendLine().AppendLine($"Count: {BadgeIcons.Dict.Count} ({totalSize:#,##0 bytes})").AppendLine();
            //builder.AppendFormat(format, "Index", "Title", "Size (Bytes)", "Example").AppendLine();
            //builder.AppendLine($"|{new string('-', 6)}:|{new string('-', 21)}:|{new string('-', 13)}:|:{new string('-', 85)}|");
            //foreach (var (key, icon) in BadgeIcons.Dict)
            //{
            //    var name = key.Length == 0 ? "empty" : key;
            //    builder.AppendFormat(format,
            //        index++, key, Encoding.UTF8.GetByteCount(icon).ToString("#,##0"), $"![{name} Icon Badge](../images/infos/icons/{name}.svg)").AppendLine();
            //}
            //this._text.AppendText(builder.ToString());

            //index = 0;
            //format = "| {0,5} | {1,7} | {2,11} | {3,20} | {4,-84} |";
            //builder.Clear().AppendLine().AppendLine($"Count: {BadgeColors.Dict.Count}").AppendLine();
            //builder.AppendFormat(format, "Index", "HEX", "RGB", "Title", "Example").AppendLine();
            //builder.AppendLine($"|{new string('-',6)}:|{new string('-', 8)}:|{new string('-', 12)}:|{new string('-', 21)}:|:{new string('-', 85)}|");
            //foreach (var (key, color) in BadgeColors.Dict)
            //{
            //    var name = key.Length == 0 ? "empty" : key;
            //    builder.AppendFormat(format,
            //        index++, BadgeUtils.ToHex(color), BadgeUtils.ToRgb(color), key, $"![{name} Color Badge](../images/infos/colors/{name}.svg)").AppendLine();
            //}
            //this._text.AppendText(builder.ToString());

#endif

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
#if NETCOREAPP

            int height;
            using (var combo = new ObjectComboBox())
            { height = (int)Math.Floor((double)combo.ItemHeight * 0.75); }

            var size = new Size(height, height);
            Helper.CacheColorImages(size);
            Helper.CacheIconImages(size);

            this.AddRow(BadgeIcons.Info, "TEXT", BadgeColors.DarkRed);
            this.AddRow(BadgeIcons.Info, "other info", BadgeColors.DarkGreen);
            this.AddRow(BadgeIcons.Info, "some long test text", BadgeColors.DarkBlue);
#endif
        }

#if NETCOREAPP

        public Badge Badge { get; } = new Badge();



        private void AddRow(BadgeIcon icon, string text, Color color)
        {
            var section = new BadgeSection(icon, text, BadgeStyle.CreateDefault(color), BadgeStyle.CreateDefault(color));
            this.Badge.Sections.Add(section);

            this._layout.SuspendLayout();
            int row = this._layout.RowCount;
            this._layout.RowCount++;
            this._layout.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            int col = 0;
            this._layout.Controls.Add(Helper.CreateRowLabel(row, $"#{row}"), col++, row);
            this._layout.Controls.Add(Helper.CreateRemoveButton(row, this.RemoveRow), col++, row);
            this._layout.Controls.Add(Helper.CreateIconCombo(row, this.ComboIcon_Changed, icon), col++, row);
            this._layout.Controls.Add(Helper.CreateColorCombo(row, this.ComboColor_Changed, color), col++, row);
            this._layout.Controls.Add(Helper.CreateContentText(row, this.TextContent_Changed, text), col++, row);
            this._layout.Controls.Add(Helper.CreatePaddingSpin(row, this.SpinPadding_Changed), col++, row);
            this._layout.ResumeLayout(true);

            this.UpdateTools();
            this.UpdatePreview();
        }

        private void RemoveRow(object sender, EventArgs e)
        { if (sender is Control control) { this.RemoveRow((int)control.Tag); } }

        private void RemoveRow(int row = -1)
        {
            // Convert -1 to index of last row with controls.
            if (row == -1) { row = this._layout.RowCount - 1; }
            // Enforce at least one row.
            if (row > 0)
            {
                this._layout.SuspendLayout();

                this.Badge.Sections.RemoveAt(row);

                for (int col = 0; col < this._layout.ColumnCount; col++)
                { this._layout.Controls.Remove(this._layout.GetControlFromPosition(col, row)); }
                this._layout.RowStyles.RemoveAt(row);

                for (row += 1; row < this._layout.RowCount; row++)
                {
                    for (int col = 0; col < this._layout.ColumnCount; col++)
                    {
                        var control = this._layout.GetControlFromPosition(col, row);
                        this._layout.SetRow(control, row - 1);
                        control.Tag = row - 1;
                        if (col == 0) { ((Label)control).Text = $"#{row - 1}"; }
                    }
                }

                this._layout.RowCount--;
                this._layout.ResumeLayout(true);

                this.UpdateTools();
                this.UpdatePreview();
            }
        }

        public void UpdateTools()
        {
            this._toolSectionRemove.Enabled = this._layout.Controls.Count > (this._layout.ColumnCount * 1);

            for (int row = 0; row < this._layout.RowCount; row++)
            {
                var removeControl = (Button)this._layout.GetControlFromPosition(1, row);
                removeControl.Enabled = row > 0;
            }
        }

        private void DrawBadge(Badge badge)
        {
            if (badge.IsNotEmpty()) { this._image.Image = badge.ToBitmap(); }
            else { this._image.Image = new Badge("Select an icon, enter text, and/or add more sections!").ToBitmap(); }
        }

#endif

        private void ComboIcon_Changed(object sender, EventArgs e)
        {
#if NETCOREAPP
            int row = (int)((Control)sender).Tag;
            var section = this.Badge.Sections[row];
            section.Icon = Helper.GetIcon((ObjectComboBox)sender);
            this.UpdatePreview();
#endif
        }

        private void ComboColor_Changed(object sender, EventArgs e)
        {
#if NETCOREAPP
            int row = (int)((Control)sender).Tag;
            var section = this.Badge.Sections[row];
            var color = Helper.GetColor((ObjectComboBox)sender);
            section.IconStyle.BackColor = color;
            section.TextStyle.BackColor = color;
            this.UpdatePreview();
#endif
        }

        private void TextContent_Changed(object sender, EventArgs e)
        {
#if NETCOREAPP
            int row = (int)((Control)sender).Tag;
            var section = this.Badge.Sections[row];
            section.Text = Helper.GetContent((TextBox)sender);
            this.UpdatePreview();
#endif
        }

        private void SpinPadding_Changed(object sender, EventArgs e)
        {
#if NETCOREAPP
            int row = (int)((Control)sender).Tag;
            var section = this.Badge.Sections[row];
            section.TextStyle.Padding = Helper.GetPadding((NumericUpDown)sender);
            this.UpdatePreview();
#endif
        }

        private void UpdatePreview(object sender, EventArgs e) => this.UpdatePreview();

        private void UpdatePreview()
        {
#if NETCOREAPP
            
            this.DrawBadge(this.Badge);

            this._textSvg.Text = this.Badge.ToTestSvg().Replace("\n", "\r\n");
            this._textBase64.Text = this.Badge.ToBase64();
            this._textJson.Text = this.Badge.ToJson();
#endif
        }

        private void ToolSectionAdd_Click(object sender, EventArgs e)
        {
#if NETCOREAPP
            this.AddRow(BadgeIcon.None, null, BadgeColors.Default);
#endif
        }

        private void ToolSectionRemove_Click(object sender, EventArgs e)
        {
#if NETCOREAPP
            this.RemoveRow();
#endif
        }

        private void ToolSaveJson_Click(object sender, EventArgs e)
        {
#if NETCOREAPP
            using var dialog = new SaveFileDialog();
            dialog.Filter = "JSON Files (*.badge;*.json)|*.badge;*.json|All files (*.*)|*.*";
            dialog.FilterIndex = 0;

            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            { File.WriteAllText(dialog.FileName, this.Badge.ToJson()); }
#endif
        }

        private void ToolSaveSvg_Click(object sender, EventArgs e)
        {
#if NETCOREAPP
            using var dialog = new SaveFileDialog();
            dialog.Filter = "SVG Files (*.svg)|*.svg|All files (*.*)|*.*";
            dialog.FilterIndex = 0;

            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            { File.WriteAllText(dialog.FileName, this.Badge.ToTestSvg()); }
#endif
        }

        private void ToolSavePng_Click(object sender, EventArgs e)
        {
#if NETCOREAPP
            using var dialog = new SaveFileDialog();
            dialog.Filter = "PNG Files (*.png)|*.png|All files (*.*)|*.*";
            dialog.FilterIndex = 0;

            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var bitmap = this.Badge.ToBitmap();
                bitmap.Save(dialog.FileName, ImageFormat.Png);
            }
#endif
        }

        private void ToolOpenJson_Click(object sender, EventArgs e)
        {
#if NETCOREAPP
#endif
        }

        private void ToolOpenText_Click(object sender, EventArgs e)
        {
#if NETCOREAPP
#endif
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
