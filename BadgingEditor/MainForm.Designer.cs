﻿namespace BadgingEditor
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._menus = new System.Windows.Forms.MenuStrip();
            this._tools = new System.Windows.Forms.ToolStrip();
            this._toolOpen = new System.Windows.Forms.ToolStripSplitButton();
            this._toolOpenJson = new System.Windows.Forms.ToolStripMenuItem();
            this._toolOpenText = new System.Windows.Forms.ToolStripMenuItem();
            this._toolSave = new System.Windows.Forms.ToolStripSplitButton();
            this._toolSaveJson = new System.Windows.Forms.ToolStripMenuItem();
            this._toolSaveSvg = new System.Windows.Forms.ToolStripMenuItem();
            this._toolSavePng = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._toolSectionAdd = new System.Windows.Forms.ToolStripButton();
            this._toolSectionRemove = new System.Windows.Forms.ToolStripButton();
            this._toolSettings = new System.Windows.Forms.ToolStripSplitButton();
            this._stats = new System.Windows.Forms.StatusStrip();
            this._split = new System.Windows.Forms.SplitContainer();
            this._layout = new System.Windows.Forms.TableLayoutPanel();
            this._tabs = new System.Windows.Forms.TabControl();
            this._tabSvg = new System.Windows.Forms.TabPage();
            this._textSvg = new System.Windows.Forms.TextBox();
            this._tabBase64 = new System.Windows.Forms.TabPage();
            this._textBase64 = new System.Windows.Forms.TextBox();
            this._tabJson = new System.Windows.Forms.TabPage();
            this._textJson = new System.Windows.Forms.TextBox();
            this._tabLog = new System.Windows.Forms.TabPage();
            this._text = new System.Windows.Forms.TextBox();
            this._image = new System.Windows.Forms.PictureBox();
            this._tools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._split)).BeginInit();
            this._split.Panel1.SuspendLayout();
            this._split.Panel2.SuspendLayout();
            this._split.SuspendLayout();
            this._tabs.SuspendLayout();
            this._tabSvg.SuspendLayout();
            this._tabBase64.SuspendLayout();
            this._tabJson.SuspendLayout();
            this._tabLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._image)).BeginInit();
            this.SuspendLayout();
            // 
            // _menus
            // 
            this._menus.ImageScalingSize = new System.Drawing.Size(24, 24);
            this._menus.Location = new System.Drawing.Point(0, 0);
            this._menus.Name = "_menus";
            this._menus.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this._menus.Size = new System.Drawing.Size(693, 24);
            this._menus.TabIndex = 0;
            this._menus.Text = "menuStrip1";
            // 
            // _tools
            // 
            this._tools.GripMargin = new System.Windows.Forms.Padding(0);
            this._tools.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tools.ImageScalingSize = new System.Drawing.Size(24, 24);
            this._tools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolOpen,
            this._toolSave,
            this.toolStripSeparator1,
            this._toolSectionAdd,
            this._toolSectionRemove,
            this._toolSettings});
            this._tools.Location = new System.Drawing.Point(0, 24);
            this._tools.Name = "_tools";
            this._tools.Size = new System.Drawing.Size(693, 31);
            this._tools.TabIndex = 1;
            this._tools.Text = "toolStrip1";
            // 
            // _toolOpen
            // 
            this._toolOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolOpenJson,
            this._toolOpenText});
            this._toolOpen.Image = ((System.Drawing.Image)(resources.GetObject("_toolOpen.Image")));
            this._toolOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolOpen.Name = "_toolOpen";
            this._toolOpen.Size = new System.Drawing.Size(40, 28);
            this._toolOpen.Text = "toolStripSplitButton1";
            // 
            // _toolOpenJson
            // 
            this._toolOpenJson.Name = "_toolOpenJson";
            this._toolOpenJson.Size = new System.Drawing.Size(111, 22);
            this._toolOpenJson.Text = "JSON...";
            this._toolOpenJson.Click += new System.EventHandler(this.ToolOpenJson_Click);
            // 
            // _toolOpenText
            // 
            this._toolOpenText.Name = "_toolOpenText";
            this._toolOpenText.Size = new System.Drawing.Size(111, 22);
            this._toolOpenText.Text = "TEXT...";
            this._toolOpenText.Click += new System.EventHandler(this.ToolOpenText_Click);
            // 
            // _toolSave
            // 
            this._toolSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolSave.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolSaveJson,
            this._toolSaveSvg,
            this._toolSavePng});
            this._toolSave.Image = ((System.Drawing.Image)(resources.GetObject("_toolSave.Image")));
            this._toolSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolSave.Name = "_toolSave";
            this._toolSave.Size = new System.Drawing.Size(40, 28);
            this._toolSave.Text = "toolStripSplitButton1";
            // 
            // _toolSaveJson
            // 
            this._toolSaveJson.Name = "_toolSaveJson";
            this._toolSaveJson.Size = new System.Drawing.Size(111, 22);
            this._toolSaveJson.Text = "JSON...";
            this._toolSaveJson.Click += new System.EventHandler(this.ToolSaveJson_Click);
            // 
            // _toolSaveSvg
            // 
            this._toolSaveSvg.Name = "_toolSaveSvg";
            this._toolSaveSvg.Size = new System.Drawing.Size(111, 22);
            this._toolSaveSvg.Text = "SVG...";
            this._toolSaveSvg.Click += new System.EventHandler(this.ToolSaveSvg_Click);
            // 
            // _toolSavePng
            // 
            this._toolSavePng.Name = "_toolSavePng";
            this._toolSavePng.Size = new System.Drawing.Size(111, 22);
            this._toolSavePng.Text = "PNG...";
            this._toolSavePng.Click += new System.EventHandler(this.ToolSavePng_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // _toolSectionAdd
            // 
            this._toolSectionAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolSectionAdd.Image = ((System.Drawing.Image)(resources.GetObject("_toolSectionAdd.Image")));
            this._toolSectionAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolSectionAdd.Name = "_toolSectionAdd";
            this._toolSectionAdd.Size = new System.Drawing.Size(28, 28);
            this._toolSectionAdd.Text = "toolStripButton1";
            this._toolSectionAdd.Click += new System.EventHandler(this.ToolSectionAdd_Click);
            // 
            // _toolSectionRemove
            // 
            this._toolSectionRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolSectionRemove.Image = ((System.Drawing.Image)(resources.GetObject("_toolSectionRemove.Image")));
            this._toolSectionRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolSectionRemove.Name = "_toolSectionRemove";
            this._toolSectionRemove.Size = new System.Drawing.Size(28, 28);
            this._toolSectionRemove.Text = "toolStripButton2";
            this._toolSectionRemove.Click += new System.EventHandler(this.ToolSectionRemove_Click);
            // 
            // _toolSettings
            // 
            this._toolSettings.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._toolSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolSettings.Image = ((System.Drawing.Image)(resources.GetObject("_toolSettings.Image")));
            this._toolSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolSettings.Name = "_toolSettings";
            this._toolSettings.Size = new System.Drawing.Size(40, 28);
            this._toolSettings.Text = "toolStripSplitButton1";
            // 
            // _stats
            // 
            this._stats.ImageScalingSize = new System.Drawing.Size(24, 24);
            this._stats.Location = new System.Drawing.Point(0, 430);
            this._stats.Name = "_stats";
            this._stats.Padding = new System.Windows.Forms.Padding(1, 0, 12, 0);
            this._stats.Size = new System.Drawing.Size(693, 22);
            this._stats.TabIndex = 2;
            this._stats.Text = "statusStrip1";
            // 
            // _split
            // 
            this._split.BackColor = System.Drawing.SystemColors.ControlDark;
            this._split.Dock = System.Windows.Forms.DockStyle.Fill;
            this._split.Location = new System.Drawing.Point(0, 79);
            this._split.Name = "_split";
            this._split.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // _split.Panel1
            // 
            this._split.Panel1.AutoScroll = true;
            this._split.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this._split.Panel1.Controls.Add(this._layout);
            // 
            // _split.Panel2
            // 
            this._split.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this._split.Panel2.Controls.Add(this._tabs);
            this._split.Size = new System.Drawing.Size(693, 351);
            this._split.SplitterDistance = 212;
            this._split.SplitterWidth = 3;
            this._split.TabIndex = 10;
            // 
            // _layout
            // 
            this._layout.AutoSize = true;
            this._layout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._layout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this._layout.ColumnCount = 6;
            this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._layout.Dock = System.Windows.Forms.DockStyle.Top;
            this._layout.Location = new System.Drawing.Point(0, 0);
            this._layout.Margin = new System.Windows.Forms.Padding(0);
            this._layout.MinimumSize = new System.Drawing.Size(0, 10);
            this._layout.Name = "_layout";
            this._layout.RowCount = 1;
            this._layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._layout.Size = new System.Drawing.Size(693, 10);
            this._layout.TabIndex = 0;
            // 
            // _tabs
            // 
            this._tabs.Controls.Add(this._tabSvg);
            this._tabs.Controls.Add(this._tabBase64);
            this._tabs.Controls.Add(this._tabJson);
            this._tabs.Controls.Add(this._tabLog);
            this._tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabs.Location = new System.Drawing.Point(0, 0);
            this._tabs.Margin = new System.Windows.Forms.Padding(0);
            this._tabs.Name = "_tabs";
            this._tabs.SelectedIndex = 0;
            this._tabs.Size = new System.Drawing.Size(693, 136);
            this._tabs.TabIndex = 0;
            // 
            // _tabSvg
            // 
            this._tabSvg.Controls.Add(this._textSvg);
            this._tabSvg.Location = new System.Drawing.Point(4, 22);
            this._tabSvg.Margin = new System.Windows.Forms.Padding(0);
            this._tabSvg.Name = "_tabSvg";
            this._tabSvg.Size = new System.Drawing.Size(685, 110);
            this._tabSvg.TabIndex = 0;
            this._tabSvg.Text = "SVG";
            this._tabSvg.UseVisualStyleBackColor = true;
            // 
            // _textSvg
            // 
            this._textSvg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._textSvg.Dock = System.Windows.Forms.DockStyle.Fill;
            this._textSvg.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._textSvg.Location = new System.Drawing.Point(0, 0);
            this._textSvg.Margin = new System.Windows.Forms.Padding(0);
            this._textSvg.Multiline = true;
            this._textSvg.Name = "_textSvg";
            this._textSvg.ReadOnly = true;
            this._textSvg.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._textSvg.Size = new System.Drawing.Size(685, 110);
            this._textSvg.TabIndex = 0;
            this._textSvg.WordWrap = false;
            // 
            // _tabBase64
            // 
            this._tabBase64.Controls.Add(this._textBase64);
            this._tabBase64.Location = new System.Drawing.Point(4, 22);
            this._tabBase64.Margin = new System.Windows.Forms.Padding(0);
            this._tabBase64.Name = "_tabBase64";
            this._tabBase64.Size = new System.Drawing.Size(685, 110);
            this._tabBase64.TabIndex = 1;
            this._tabBase64.Text = "BASE64";
            this._tabBase64.UseVisualStyleBackColor = true;
            // 
            // _textBase64
            // 
            this._textBase64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._textBase64.Dock = System.Windows.Forms.DockStyle.Fill;
            this._textBase64.Font = new System.Drawing.Font("Consolas", 8.25F);
            this._textBase64.Location = new System.Drawing.Point(0, 0);
            this._textBase64.Margin = new System.Windows.Forms.Padding(0);
            this._textBase64.Multiline = true;
            this._textBase64.Name = "_textBase64";
            this._textBase64.ReadOnly = true;
            this._textBase64.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._textBase64.Size = new System.Drawing.Size(685, 110);
            this._textBase64.TabIndex = 1;
            this._textBase64.WordWrap = false;
            // 
            // _tabJson
            // 
            this._tabJson.Controls.Add(this._textJson);
            this._tabJson.Location = new System.Drawing.Point(4, 22);
            this._tabJson.Margin = new System.Windows.Forms.Padding(0);
            this._tabJson.Name = "_tabJson";
            this._tabJson.Size = new System.Drawing.Size(685, 110);
            this._tabJson.TabIndex = 2;
            this._tabJson.Text = "JSON";
            this._tabJson.UseVisualStyleBackColor = true;
            // 
            // _textJson
            // 
            this._textJson.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._textJson.Dock = System.Windows.Forms.DockStyle.Fill;
            this._textJson.Font = new System.Drawing.Font("Consolas", 8.25F);
            this._textJson.Location = new System.Drawing.Point(0, 0);
            this._textJson.Margin = new System.Windows.Forms.Padding(0);
            this._textJson.Multiline = true;
            this._textJson.Name = "_textJson";
            this._textJson.ReadOnly = true;
            this._textJson.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._textJson.Size = new System.Drawing.Size(685, 110);
            this._textJson.TabIndex = 1;
            // 
            // _tabLog
            // 
            this._tabLog.Controls.Add(this._text);
            this._tabLog.Location = new System.Drawing.Point(4, 22);
            this._tabLog.Margin = new System.Windows.Forms.Padding(0);
            this._tabLog.Name = "_tabLog";
            this._tabLog.Size = new System.Drawing.Size(685, 110);
            this._tabLog.TabIndex = 3;
            this._tabLog.Text = "Log";
            this._tabLog.UseVisualStyleBackColor = true;
            // 
            // _text
            // 
            this._text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._text.Dock = System.Windows.Forms.DockStyle.Fill;
            this._text.Font = new System.Drawing.Font("Consolas", 8.25F);
            this._text.Location = new System.Drawing.Point(0, 0);
            this._text.Margin = new System.Windows.Forms.Padding(0);
            this._text.Multiline = true;
            this._text.Name = "_text";
            this._text.ReadOnly = true;
            this._text.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._text.Size = new System.Drawing.Size(685, 110);
            this._text.TabIndex = 1;
            // 
            // _image
            // 
            this._image.BackColor = System.Drawing.SystemColors.Control;
            this._image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._image.Dock = System.Windows.Forms.DockStyle.Top;
            this._image.Location = new System.Drawing.Point(0, 55);
            this._image.Margin = new System.Windows.Forms.Padding(0);
            this._image.MinimumSize = new System.Drawing.Size(2, 24);
            this._image.Name = "_image";
            this._image.Size = new System.Drawing.Size(693, 24);
            this._image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this._image.TabIndex = 0;
            this._image.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 452);
            this.Controls.Add(this._split);
            this.Controls.Add(this._image);
            this.Controls.Add(this._stats);
            this.Controls.Add(this._tools);
            this.Controls.Add(this._menus);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this._tools.ResumeLayout(false);
            this._tools.PerformLayout();
            this._split.Panel1.ResumeLayout(false);
            this._split.Panel1.PerformLayout();
            this._split.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._split)).EndInit();
            this._split.ResumeLayout(false);
            this._tabs.ResumeLayout(false);
            this._tabSvg.ResumeLayout(false);
            this._tabSvg.PerformLayout();
            this._tabBase64.ResumeLayout(false);
            this._tabBase64.PerformLayout();
            this._tabJson.ResumeLayout(false);
            this._tabJson.PerformLayout();
            this._tabLog.ResumeLayout(false);
            this._tabLog.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox _image;
        private System.Windows.Forms.TextBox _text;
        private System.Windows.Forms.MenuStrip _menus;
        private System.Windows.Forms.ToolStrip _tools;
        private System.Windows.Forms.StatusStrip _stats;
        private System.Windows.Forms.SplitContainer _split;
        private System.Windows.Forms.TableLayoutPanel _layout;
        private System.Windows.Forms.ToolStripButton _toolSectionAdd;
        private System.Windows.Forms.ToolStripButton _toolSectionRemove;
        private System.Windows.Forms.ToolStripSplitButton _toolSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _toolSaveJson;
        private System.Windows.Forms.ToolStripMenuItem _toolSaveSvg;
        private System.Windows.Forms.ToolStripMenuItem _toolSavePng;
        private System.Windows.Forms.ToolStripSplitButton _toolOpen;
        private System.Windows.Forms.ToolStripMenuItem _toolOpenJson;
        private System.Windows.Forms.ToolStripMenuItem _toolOpenText;
        private System.Windows.Forms.TabControl _tabs;
        private System.Windows.Forms.TabPage _tabSvg;
        private System.Windows.Forms.TextBox _textSvg;
        private System.Windows.Forms.TabPage _tabBase64;
        private System.Windows.Forms.TextBox _textBase64;
        private System.Windows.Forms.TabPage _tabJson;
        private System.Windows.Forms.TextBox _textJson;
        private System.Windows.Forms.TabPage _tabLog;
        private System.Windows.Forms.ToolStripSplitButton _toolSettings;
    }
}

