﻿# Frequently Asked Questions

## How to put spaces in badge section labels?

The simple answer is to wrap the section with double-quote characters.

```
badgingbadger "some text with spaces default color and no icon::"
badgingbadger "some text with spaces and no icon:red:"
badgingbadger "some text with spaces:red:info"
```

To be more specific, the operating system parses the command line string into arguments and the Badger then processes the list of arguments it is given. Most command line parsers use double-quotes to indicate an argument with whitespace, but some systems may be different.

## How to include colons in badge section labels?

As long as the section's color and icon elements are defined, there shouldn't be any additional work required. If you want colons in the label, the default color, and no icon, then simply append two additional colons to the end of the label.

```
badgingbadger label:with:colons::
badgingbadger label:with:colons:red:
badgingbadger label:with:colons:red:info
```

## How to have a blank badge section label?

Leave the label element blank. If you are defining a color and or icon, don't forget to include the leading colon.
