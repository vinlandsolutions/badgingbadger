﻿# Command Examples

Here are some basic examples of badges created by the Badging Badger. For examples of specific commands, please the other pages in this section.

| Command | Badge |
|---------|:------|
| `badgingbadger :default:info "Some Information:blue"` | ![Badge Standard](../images/badge_standard.svg) |
| `badgingbadger "label text:orange:info"` | ![Badge Simple](../images/badge_simple.svg) |
| `badgingbadger :default:info "Some Information:blue" value:with:colons:red:` | ![Badge Standard](../images/badge_multisection.svg) |
| `badgingbadger tests:: "✔ 100 | ✘ 0:bgreen:" 74.28%:byellow:` | ![Badge Unit Tests](../images/badge_tests_and_coverage.svg) |
| `badgingbadger bgreen:bgreen:`          | ![Badge Simple](../images/badge_color_bgreen.svg) |
| `badgingbadger bblue:bblue:`            | ![Badge Simple](../images/badge_color_bblue.svg) |
| `badgingbadger byellow:byellow:`        | ![Badge Simple](../images/badge_color_byellow.svg) |
| `badgingbadger borange:borange:`        | ![Badge Simple](../images/badge_color_borange.svg) |
| `badgingbadger bred:bred:`              | ![Badge Simple](../images/badge_color_bred.svg) |
| `badgingbadger blightgray:blightgray:` | ![Badge Simple](../images/badge_color_blightgray.svg) |
| `badgingbadger bdarkgray:bdarkgray:`   | ![Badge Simple](../images/badge_color_bdarkgray.svg) |
