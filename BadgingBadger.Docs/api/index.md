# Vinland Solutions - The Badging Badger
> Automate Svg Badge Generation

[![License](https://vinlandsolutions.gitlab.io/badgingbadger/badge/license.svg)](https://vinlandsolutions.gitlab.io/badgingbadger/license.html)
[![Platform](https://vinlandsolutions.gitlab.io/badgingbadger/badge/platform.svg)](https://docs.microsoft.com/en-us/dotnet/core/)
[![Repository](https://vinlandsolutions.gitlab.io/badgingbadger/badge/repository.svg)](https://gitlab.com/vinlandsolutions/badgingbadger)
[![Releases](https://vinlandsolutions.gitlab.io/badgingbadger/badge/releases.svg)](https://gitlab.com/vinlandsolutions/badgingbadger/-/releases)
[![Documentation](https://vinlandsolutions.gitlab.io/badgingbadger/badge/documentation.svg)](https://vinlandsolutions.gitlab.io/badgingbadger/)  
[![Nuget](https://badgen.net/nuget/v/dotnet-badgingbadger/latest?icon)](https://www.nuget.org/packages/dotnet-badgingbadger/)
[![Pipeline](https://gitlab.com/vinlandsolutions/badgingbadger/badges/master/pipeline.svg)](https://gitlab.com/vinlandsolutions/badgingbadger/pipelines)
[![Coverage](https://gitlab.com/vinlandsolutions/badgingbadger/badges/master/coverage.svg)](https://vinlandsolutions.gitlab.io/badgingbadger/reports/index.html)
[![Tests](https://vinlandsolutions.gitlab.io/badgingbadger/badge/tests.svg)](https://vinlandsolutions.gitlab.io/badgingbadger/reports/index.html)

The **Badging Badger** is a dotnet console tool for generating svg badges similar to those used by code repositories.

Most badge generation tools limit the badge to two sections, one with a color, a label, and an optional icon, and the other with only a color and a label. The Badger, however, has no such limitation and is able to handle any number of sections, each with its own color, label, and optional icon.

## Installation

The official release versions of the **Badging Badger** are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.BadgingBadger/) and can be installed as a global or local [dotnet tool](https://docs.microsoft.com/en-us/dotnet/core/tools/global-tools).

```
dotnet tool install --global dotnet-badging-badger
dotnet tool install dotnet-badging-badger --tool-path tools
```

## Usage

```
The Badging Badger 1.0.0-pre.0 (DEBUG)
Copyright (c) 2019 Vinland Solutions
MIT License (MIT): https://opensource.org/licenses/MIT

The Badging Badger is a dotnet console tool for generating svg badges similar to
those used by code repositories.

USAGE:
  badgingbadger (--colors | --icons | --help | --license | --version)
  badgingbadger [options] ([label]:[color]:[icon])
  badgingbadger [options] ("[label with spaces]:[color]:[icon]")
  badgingbadger [options] <section> [<section> ...]

OPTIONS:
    -f, --format         (Default: Svg) Select format type used to write the
                         badge. Valid values: Svg, Base64
    -o, --output         Write the badge output to the given path instead of
                         stdout.
    -v, --verbose        Print additional information to stdout.
    --colors             Print the list of built-in colors.
    --icons              Print the list of built-in icons.
    --help               Print the program help screen.
    --license            Print the full license information.
    --version            Print the program version.
    sections (pos. 0)    Space-separated list of colon-delimited section values.
                         "text:color:icon"
```

### Input

Each badge section is defined by a colon-delimited set of three elements: label, color, and icon. For example, `OS:red:windows`.

Multiple sections can be declared as a space-delimited sequence of `label:color:icon` elements. For example, `::info docs:bblue:`.

#### Label Element

Labels can contain whitespace by wrapping the entire section in double-quote characters, eg `"quoted section:red:info"`. The label can contain colons as long as both the color and the icon elements are included in the section, eg `some:label:red:info`, `some:label:red:`, and `some:label::`.

#### Color Element

The color element can be declared in several ways.

1. As a three- or six-digit hex value prefixed by the hash/sharp/number symbol, eg `#FFF` or `#FFFFFF`.
2. As a comma-delimited three-value rbg sequence, eg `0,0,255`.
3. As one of the [named colors](https://commoncorelibs.gitlab.io/commoncore-badging/api/CommonCore.Badging.BadgeColors.html) provided by the underlying [CommonCore.Badging library](https://gitlab.com/commoncorelibs/commoncore-badging).
4. The element can be left blank, indicating the default color (#555555).
5. The default color can also be invoked as a named color using `default`.
6. The Badger provides six additional named colors to support the commonly expected badge colors.

| Color Name | Hex Value |
|------------|----------:|
| bgreen     |   #33CC11 |
| bblue      |   #0088CC |
| byellow    |   #DDBB11 |
| borange    |   #FF7733 |
| bred       |   #EE4433 |
| blightgray |   #999999 |
| bdarkgray  |   #555555 |

#### Icon Element

The icon element can be declared in several ways.

1. As a url, eg `https://simpleicons.org/icons/dot-net.svg`.
2. As a local file path that must use the `svg` extension, eg `some-icon.svg` or `../icons/other-icon.svg`.
3. As one of the [named icons](https://commoncorelibs.gitlab.io/commoncore-badging/api/CommonCore.Badging.BadgeIcons.html) provided by the underlying [CommonCore.Badging library](https://gitlab.com/commoncorelibs/commoncore-badging).
4. The element can be left blank, indicating the section does not have an icon.

### Output

By default the tool will print the text of the generated svg badge to the standard output stream. However the output can be redirected to a file by specifying an appropriate path to the `--output` option. The file will be created if it doesn't exist, but the specified directory of the path must exist. Specifying the `--verbose` flag will print additional information to the standard output stream. Finally, the svg badge can be encoded into base64 before output with `--format` option. If any errors are encountered, the error screen is displayed on the standard error stream and the application exits with non-zero code.

## Projects

The BadgingBadger repository is composed of four projects with the listed dependencies:

* **BadgingBadger**: The dotnet core console application.
  * [System.CommandLine](https://github.com/dotnet/command-line-api)
  * [CommonCore.CommandLine](https://gitlab.com/commoncorelibs/commoncore-commandline)
  * [CommonCore.Badging](https://gitlab.com/commoncorelibs/commoncore-badging)
* **BadgingEditor**: The dotnet core Windows Forms gui application.
  * [Svg](https://github.com/vvvv/SVG) and [Fizzler](https://github.com/atifaziz/Fizzler)
  * [CommonCore.Badging](https://gitlab.com/commoncorelibs/commoncore-badging)
* **BadgingBadger.Docs**: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* **BadgingBadger.Tests**: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/vinlandsolutions/badgingbadger
- Issues: https://gitlab.com/vinlandsolutions/badgingbadger/issues
- Docs: https://vinlandsolutions.gitlab.io/badgingbadger/index.html
- Nuget: https://www.nuget.org/packages/dotnet-badgingbadger/
