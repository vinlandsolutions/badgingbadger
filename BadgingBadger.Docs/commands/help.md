﻿# Help Command

Print the program help screen.

## Standard Usage

### Input

```
badgingbadger --help
```

### Output

```
The Badging Badger 1.0.0-pre.0 (DEBUG)
Copyright (c) 2019 Vinland Solutions
MIT License (MIT): https://opensource.org/licenses/MIT

The Badging Badger is a dotnet console tool for generating svg badges similar to
those used by code repositories.

USAGE:
  badgingbadger (--colors | --icons | --help | --license | --version)
  badgingbadger [options] ([label]:[color]:[icon])
  badgingbadger [options] ("[label with spaces]:[color]:[icon]")
  badgingbadger [options] <section> [<section> ...]

OPTIONS:
    -f, --format         (Default: Svg) Select format type used to write the
                         badge. Valid values: Svg, Base64
    -o, --output         Write the badge output to the given path instead of
                         stdout.
    -v, --verbose        Print additional information to stdout.
    --colors             Print the list of built-in colors.
    --icons              Print the list of built-in icons.
    --help               Print the program help screen.
    --license            Print the full license information.
    --version            Print the program version.
    sections (pos. 0)    Space-separated list of colon-delimited section values.
                         "label:color:icon"

```
