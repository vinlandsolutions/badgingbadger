﻿# Colors Command

Print the list of built-in colors.

## Standard Usage

### Input

```
badgingbadger --colors
```

### Output

```
The Badging Badger 1.0.0-pre.0 (DEBUG)

Count: 142
|       |      HEX  |          RGB  |                 Title  |
|    0  |  #555555  |     85,85,85  |                        |
|    1  |  #555555  |     85,85,85  |               default  |
|    2  |  #F0F8FF  |  240,248,255  |             aliceblue  |
|    3  |  #FAEBD7  |  250,235,215  |          antiquewhite  |
|    4  |  #00FFFF  |    0,255,255  |                  aqua  |
|    5  |  #7FFFD4  |  127,255,212  |            aquamarine  |
|    6  |  #F0FFFF  |  240,255,255  |                 azure  |
|    7  |  #F5F5DC  |  245,245,220  |                 beige  |
|    8  |  #FFE4C4  |  255,228,196  |                bisque  |
|    9  |  #000000  |        0,0,0  |                 black  |
|   10  |  #FFEBCD  |  255,235,205  |        blanchedalmond  |
|   11  |  #0000FF  |      0,0,255  |                  blue  |
|   12  |  #8A2BE2  |   138,43,226  |            blueviolet  |
|   13  |  #A52A2A  |    165,42,42  |                 brown  |
|   14  |  #DEB887  |  222,184,135  |             burlywood  |
|   15  |  #5F9EA0  |   95,158,160  |             cadetblue  |
|   16  |  #7FFF00  |    127,255,0  |            chartreuse  |
|   17  |  #D2691E  |   210,105,30  |             chocolate  |
|   18  |  #FF7F50  |   255,127,80  |                 coral  |
|   19  |  #6495ED  |  100,149,237  |        cornflowerblue  |
|   20  |  #FFF8DC  |  255,248,220  |              cornsilk  |
|   21  |  #DC143C  |    220,20,60  |               crimson  |
|   22  |  #00FFFF  |    0,255,255  |                  cyan  |
|   23  |  #00008B  |      0,0,139  |              darkblue  |
|   24  |  #008B8B  |    0,139,139  |              darkcyan  |
|   25  |  #B8860B  |   184,134,11  |         darkgoldenrod  |
|   26  |  #A9A9A9  |  169,169,169  |              darkgray  |
|   27  |  #006400  |      0,100,0  |             darkgreen  |
|   28  |  #BDB76B  |  189,183,107  |             darkkhaki  |
|   29  |  #8B008B  |    139,0,139  |           darkmagenta  |
|   30  |  #556B2F  |    85,107,47  |        darkolivegreen  |
|   31  |  #FF8C00  |    255,140,0  |            darkorange  |
|   32  |  #9932CC  |   153,50,204  |            darkorchid  |
|   33  |  #8B0000  |      139,0,0  |               darkred  |
|   34  |  #E9967A  |  233,150,122  |            darksalmon  |
|   35  |  #8FBC8B  |  143,188,139  |          darkseagreen  |
|   36  |  #483D8B  |    72,61,139  |         darkslateblue  |
|   37  |  #2F4F4F  |     47,79,79  |         darkslategray  |
|   38  |  #00CED1  |    0,206,209  |         darkturquoise  |
|   39  |  #9400D3  |    148,0,211  |            darkviolet  |
|   40  |  #FF1493  |   255,20,147  |              deeppink  |
|   41  |  #00BFFF  |    0,191,255  |           deepskyblue  |
|   42  |  #696969  |  105,105,105  |               dimgray  |
|   43  |  #1E90FF  |   30,144,255  |            dodgerblue  |
|   44  |  #B22222  |    178,34,34  |             firebrick  |
|   45  |  #FFFAF0  |  255,250,240  |           floralwhite  |
|   46  |  #228B22  |    34,139,34  |           forestgreen  |
|   47  |  #FF00FF  |    255,0,255  |               fuchsia  |
|   48  |  #DCDCDC  |  220,220,220  |             gainsboro  |
|   49  |  #F8F8FF  |  248,248,255  |            ghostwhite  |
|   50  |  #FFD700  |    255,215,0  |                  gold  |
|   51  |  #DAA520  |   218,165,32  |             goldenrod  |
|   52  |  #808080  |  128,128,128  |                  gray  |
|   53  |  #008000  |      0,128,0  |                 green  |
|   54  |  #ADFF2F  |   173,255,47  |           greenyellow  |
|   55  |  #F0FFF0  |  240,255,240  |              honeydew  |
|   56  |  #FF69B4  |  255,105,180  |               hotpink  |
|   57  |  #CD5C5C  |    205,92,92  |             indianred  |
|   58  |  #4B0082  |     75,0,130  |                indigo  |
|   59  |  #FFFFF0  |  255,255,240  |                 ivory  |
|   60  |  #F0E68C  |  240,230,140  |                 khaki  |
|   61  |  #E6E6FA  |  230,230,250  |              lavender  |
|   62  |  #FFF0F5  |  255,240,245  |         lavenderblush  |
|   63  |  #7CFC00  |    124,252,0  |             lawngreen  |
|   64  |  #FFFACD  |  255,250,205  |          lemonchiffon  |
|   65  |  #ADD8E6  |  173,216,230  |             lightblue  |
|   66  |  #F08080  |  240,128,128  |            lightcoral  |
|   67  |  #E0FFFF  |  224,255,255  |             lightcyan  |
|   68  |  #FAFAD2  |  250,250,210  |  lightgoldenrodyellow  |
|   69  |  #D3D3D3  |  211,211,211  |             lightgray  |
|   70  |  #90EE90  |  144,238,144  |            lightgreen  |
|   71  |  #FFB6C1  |  255,182,193  |             lightpink  |
|   72  |  #FFA07A  |  255,160,122  |           lightsalmon  |
|   73  |  #20B2AA  |   32,178,170  |         lightseagreen  |
|   74  |  #87CEFA  |  135,206,250  |          lightskyblue  |
|   75  |  #778899  |  119,136,153  |        lightslategray  |
|   76  |  #B0C4DE  |  176,196,222  |        lightsteelblue  |
|   77  |  #FFFFE0  |  255,255,224  |           lightyellow  |
|   78  |  #00FF00  |      0,255,0  |                  lime  |
|   79  |  #32CD32  |    50,205,50  |             limegreen  |
|   80  |  #FAF0E6  |  250,240,230  |                 linen  |
|   81  |  #FF00FF  |    255,0,255  |               magenta  |
|   82  |  #800000  |      128,0,0  |                maroon  |
|   83  |  #66CDAA  |  102,205,170  |      mediumaquamarine  |
|   84  |  #0000CD  |      0,0,205  |            mediumblue  |
|   85  |  #BA55D3  |   186,85,211  |          mediumorchid  |
|   86  |  #9370DB  |  147,112,219  |          mediumpurple  |
|   87  |  #3CB371  |   60,179,113  |        mediumseagreen  |
|   88  |  #7B68EE  |  123,104,238  |       mediumslateblue  |
|   89  |  #00FA9A  |    0,250,154  |     mediumspringgreen  |
|   90  |  #48D1CC  |   72,209,204  |       mediumturquoise  |
|   91  |  #C71585  |   199,21,133  |       mediumvioletred  |
|   92  |  #191970  |    25,25,112  |          midnightblue  |
|   93  |  #F5FFFA  |  245,255,250  |             mintcream  |
|   94  |  #FFE4E1  |  255,228,225  |             mistyrose  |
|   95  |  #FFE4B5  |  255,228,181  |              moccasin  |
|   96  |  #FFDEAD  |  255,222,173  |           navajowhite  |
|   97  |  #000080  |      0,0,128  |                  navy  |
|   98  |  #FDF5E6  |  253,245,230  |               oldlace  |
|   99  |  #808000  |    128,128,0  |                 olive  |
|  100  |  #6B8E23  |   107,142,35  |             olivedrab  |
|  101  |  #FFA500  |    255,165,0  |                orange  |
|  102  |  #FF4500  |     255,69,0  |             orangered  |
|  103  |  #DA70D6  |  218,112,214  |                orchid  |
|  104  |  #EEE8AA  |  238,232,170  |         palegoldenrod  |
|  105  |  #98FB98  |  152,251,152  |             palegreen  |
|  106  |  #AFEEEE  |  175,238,238  |         paleturquoise  |
|  107  |  #DB7093  |  219,112,147  |         palevioletred  |
|  108  |  #FFEFD5  |  255,239,213  |            papayawhip  |
|  109  |  #FFDAB9  |  255,218,185  |             peachpuff  |
|  110  |  #CD853F  |   205,133,63  |                  peru  |
|  111  |  #FFC0CB  |  255,192,203  |                  pink  |
|  112  |  #DDA0DD  |  221,160,221  |                  plum  |
|  113  |  #B0E0E6  |  176,224,230  |            powderblue  |
|  114  |  #800080  |    128,0,128  |                purple  |
|  115  |  #FF0000  |      255,0,0  |                   red  |
|  116  |  #BC8F8F  |  188,143,143  |             rosybrown  |
|  117  |  #4169E1  |   65,105,225  |             royalblue  |
|  118  |  #8B4513  |    139,69,19  |           saddlebrown  |
|  119  |  #FA8072  |  250,128,114  |                salmon  |
|  120  |  #F4A460  |   244,164,96  |            sandybrown  |
|  121  |  #2E8B57  |    46,139,87  |              seagreen  |
|  122  |  #FFF5EE  |  255,245,238  |              seashell  |
|  123  |  #A0522D  |    160,82,45  |                sienna  |
|  124  |  #C0C0C0  |  192,192,192  |                silver  |
|  125  |  #87CEEB  |  135,206,235  |               skyblue  |
|  126  |  #6A5ACD  |   106,90,205  |             slateblue  |
|  127  |  #708090  |  112,128,144  |             slategray  |
|  128  |  #FFFAFA  |  255,250,250  |                  snow  |
|  129  |  #00FF7F  |    0,255,127  |           springgreen  |
|  130  |  #4682B4  |   70,130,180  |             steelblue  |
|  131  |  #D2B48C  |  210,180,140  |                   tan  |
|  132  |  #008080  |    0,128,128  |                  teal  |
|  133  |  #D8BFD8  |  216,191,216  |               thistle  |
|  134  |  #FF6347  |    255,99,71  |                tomato  |
|  135  |  #40E0D0  |   64,224,208  |             turquoise  |
|  136  |  #EE82EE  |  238,130,238  |                violet  |
|  137  |  #F5DEB3  |  245,222,179  |                 wheat  |
|  138  |  #FFFFFF  |  255,255,255  |                 white  |
|  139  |  #F5F5F5  |  245,245,245  |            whitesmoke  |
|  140  |  #FFFF00  |    255,255,0  |                yellow  |
|  141  |  #9ACD32  |   154,205,50  |           yellowgreen  |

```
