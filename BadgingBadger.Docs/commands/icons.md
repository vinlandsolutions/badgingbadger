﻿# Icons Command

Print the list of built-in icons.

## Standard Usage

### Input

```
badgingbadger --icons
```

### Output

```
The Badging Badger 1.0.0-pre.0 (DEBUG)

Count: 142 (140,092 bytes)
|       |             Title  |  Size (Bytes)  |
|    0  |            airbnb  |  1,008  |
|    1  |            amazon  |  2,856  |
|    2  |         amazonaws  |  4,176  |
|    3  |           android  |  1,412  |
|    4  |            apache  |  4,500  |
|    5  |             apple  |    692  |
|    6  |          appveyor  |    600  |
|    7  |              atom  |  2,236  |
|    8  |           audible  |    832  |
|    9  |           awesome  |    400  |
|   10  |           bitcoin  |  1,188  |
|   11  |           blender  |  1,704  |
|   12  |      buymeacoffee  |    536  |
|   13  |          circleci  |    720  |
|   14  |          codebeat  |    616  |
|   15  |       codeclimate  |    312  |
|   16  |           codecov  |  1,496  |
|   17  |          codeship  |  1,660  |
|   18  |          commonwl  |    548  |
|   19  |   creativecommons  |  1,552  |
|   20  |              css3  |    392  |
|   21  |          deepscan  |  1,268  |
|   22  |        dependabot  |  1,308  |
|   23  |           discord  |  2,384  |
|   24  |            disqus  |    636  |
|   25  |           dockbit  |  1,288  |
|   26  |            docker  |  1,140  |
|   27  |            dotnet  |    956  |
|   28  |           dropbox  |    420  |
|   29  |              ebay  |  1,676  |
|   30  |           eclipse  |  1,016  |
|   31  |          facebook  |    628  |
|   32  |         filezilla  |  3,320  |
|   33  |              flow  |  1,032  |
|   34  |               git  |    932  |
|   35  |            github  |    908  |
|   36  |            gitlab  |    372  |
|   37  |            gitpod  |    420  |
|   38  |            gitter  |    400  |
|   39  |             gmail  |    404  |
|   40  |            google  |    520  |
|   41  |      googlechrome  |    864  |
|   42  |        googleplay  |    632  |
|   43  |           graphql  |    920  |
|   44  |          gravatar  |    568  |
|   45  |           haskell  |    336  |
|   46  |             html5  |    420  |
|   47  |              imdb  |  1,812  |
|   48  |              info  |  1,240  |
|   49  |          inkscape  |  1,416  |
|   50  |         instagram  |  2,388  |
|   51  |  internetexplorer  |  1,296  |
|   52  |              java  |  1,684  |
|   53  |        javascript  |  1,272  |
|   54  |            jquery  |  4,540  |
|   55  |              json  |  3,440  |
|   56  |              kofi  |    712  |
|   57  |        kubernetes  |  4,728  |
|   58  |              lgtm  |  4,812  |
|   59  |         libraries  |  1,008  |
|   60  |             linux  |  7,572  |
|   61  |   linuxfoundation  |    224  |
|   62  |          markdown  |    528  |
|   63  |              mega  |    732  |
|   64  |           mozilla  |  1,712  |
|   65  |    mozillafirefox  |  2,832  |
|   66  |               now  |    232  |
|   67  |               npm  |  1,708  |
|   68  |             nuget  |  1,044  |
|   69  |           palette  |    720  |
|   70  |           patreon  |    392  |
|   71  |            paypal  |  1,504  |
|   72  |               php  |  1,484  |
|   73  |        postgresql  |  2,348  |
|   74  |        powershell  |    828  |
|   75  |              pypi  |  2,552  |
|   76  |            python  |  2,040  |
|   77  |               rss  |    580  |
|   78  |              ruby  |    596  |
|   79  |       scrutinizer  |  1,224  |
|   80  |             slack  |  1,180  |
|   81  |       sourceforge  |  1,120  |
|   82  |       sourcegraph  |    964  |
|   83  |     stackoverflow  |    588  |
|   84  |             steam  |  1,364  |
|   85  |          telegram  |    904  |
|   86  |          terminal  |    364  |
|   87  |               tor  |  3,604  |
|   88  |            travis  |  7,544  |
|   89  |           twitter  |    884  |
|   90  |        typescript  |    796  |
|   91  |             unity  |    416  |
|   92  |  visualstudiocode  |    608  |
|   93  |    vlcmediaplayer  |  1,024  |
|   94  |         wikipedia  |  1,856  |
|   95  |           windows  |    384  |
|   96  |           youtube  |    644  |
|   97  |              zeit  |    444  |

```
