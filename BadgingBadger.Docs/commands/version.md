﻿# Version Command

Print the program version.

## Standard Usage

### Input

```
badgingbadger --version
```

### Output

```
The Badging Badger 1.0.0-pre.0 (DEBUG)

```
