﻿# Badge Command

Space-separated list of colon-delimited section values.

## Standard Usage

### Input

```
badgingbadger ::info "Some Information:blue:"
```

### Output

![Badge Standard](../images/badge_standard.svg)

```
<?xml version="1.0" encoding="UTF-8"?>
<svg width="126" height="20" xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink">
    <linearGradient id="b" x2="0" y2="100%">
        <stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
        <stop offset="1" stop-opacity=".1"/>
    </linearGradient>
    <mask id="badge">
        <rect width="126" height="20" rx="3" fill="#FFFFFF"/>
    </mask>
    <g mask="url(#badge)">
        <path fill="#555555" d="M0 0h20v20h-20z"/>
        <path fill="#0000FF" d="M20 0h106v20h-106z"/>
        <path fill="url(#b)" d="M0 0h126v20H0z"/>
    </g>
    <image x="3" y="3" width="14" height="14" viewBox="0 0 14 14"
    xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcm
    cvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+CiAgPHBhdGggZD0iTTIzLjkyMTg3NSAxMi
    4wMTk1MzFjLS4wMDc4MTMgNi40OTIxODgtNS4yNDYwOTQgMTEuODAwNzgxLTExLjgwNDY4NyAxMS
    43OTY4NzVDNS42MDE1NjIgMjMuODEyNS4zMjQyMTkgMTguNTU0Njg4LjMyNDIxOSAxMi4wMTE3MT
    kuMzI4MTI1IDUuNDYwOTM4IDUuNjI4OTA2LjIyMjY1NiAxMi4xMjUuMjE4NzVjNi41MTE3MTkgMC
    AxMS43OTY4NzUgNS4yODkwNjIgMTEuNzk2ODc1IDExLjgwMDc4MXptLTkuNzYxNzE5IDIuNTgyMD
    MxYzAtMS40NTMxMjQgMC0yLjkwNjI1LjAwMzkwNi00LjM1OTM3NCAwLS41NTA3ODItLjE5NTMxMi
    0xLjAxNTYyNi0uNjAxNTYyLTEuMzc4OTA3LS42MzY3MTktLjU3MDMxMi0xLjM4MjgxMi0uNjkxND
    A2LTIuMTc5Njg4LS40MTAxNTYtLjc2MTcxOC4yNjk1MzEtMS4yNzM0MzcgMS4wMDM5MDYtMS4yNj
    k1MzEgMS44MzIwMzEuMDE1NjI1IDIuODk0NTMyLjAwNzgxMyA1Ljc4OTA2My4wMDM5MDcgOC42OD
    c1IDAgMS42MzY3MTkgMS40MDIzNDMgMS45MTAxNTYgMi4wNzQyMTggMS45MTAxNTYgMS4wMDc4MT
    MtLjAwMzkwNiAxLjk5NjA5NC0uODE2NDA2IDEuOTc2NTYzLTEuOTg0Mzc0LS4wMjczNDQtMS40Mj
    k2ODgtLjAwNzgxMy0yLjg2MzI4Mi0uMDA3ODEzLTQuMjk2ODc2em0uMTQwNjI1LTkuMjkyOTY4Yy
    0uMDAzOTA2LTEuMjA3MDMyLS45NjQ4NDMtMi4xNjc5NjktMi4xNzU3ODEtMi4xNjc5NjktMS4yMT
    Q4NDQgMC0yLjE3OTY4OC45Njg3NS0yLjE3OTY4OCAyLjE3OTY4Ny4wMDM5MDcgMS4xOTkyMTkuOT
    gwNDY5IDIuMTc1NzgyIDIuMTc5Njg4IDIuMTc1NzgyIDEuMjEwOTM4IDAgMi4xNzU3ODEtLjk3Mj
    Y1NiAyLjE3NTc4MS0yLjE4NzV6bTAgMCIgZmlsbD0iI2ZmZiIvPgo8L3N2Zz4K" />
    <g fill="#FFFFFF" text-anchor="middle" font-family="DejaVu
    Sans,Verdana,Geneva,sans-serif" font-size="11">
        <text x="21" y="15" fill="#010101" fill-opacity=".3"></text>
        <text x="20" y="14"></text>
    </g>
    <g fill="#FFFFFF" text-anchor="middle" font-family="DejaVu
    Sans,Verdana,Geneva,sans-serif" font-size="11">
        <text x="74" y="15" fill="#010101" fill-opacity=".3">Some
        Information</text>
        <text x="73" y="14">Some Information</text>
    </g>
</svg>

```

## Simple Usage (Verbose)

### Input

```
badgingbadger --verbose "label text:orange:info"
```

### Output

![Badge Simple](../images/badge_simple.svg)

```
The Badging Badger 1.0.0-pre.0 (DEBUG)

SETTING(S):
  Verbose: True
  Format: Svg
  Output: <stdout>

SECTION(S):
  Count: 1
  |     |        Text  |    Color  |                        Icon  |
  |  0  |  label text  |  #FFA500  |  PHN2ZyB4bWxucz0iaHR0cDovL3  |
  |     |              |           |  d3dy53My5vcmcvMjAwMC9zdmci  |
  |     |              |           |  IHZpZXdCb3g9IjAgMCAyNCAyNC  |
  |     |              |           |  I+CiAgPHBhdGggZD0iTTIzLjky  |
  |     |              |           |  MTg3NSAxMi4wMTk1MzFjLS4wMD  |
  |     |              |           |  c4MTMgNi40OTIxODgtNS4yNDYw  |
  |     |              |           |  OTQgMTEuODAwNzgxLTExLjgwND  |
  |     |              |           |  Y4NyAxMS43OTY4NzVDNS42MDE1  |
  |     |              |           |  NjIgMjMuODEyNS4zMjQyMTkgMT  |
  |     |              |           |  guNTU0Njg4LjMyNDIxOSAxMi4w  |
  |     |              |           |  MTE3MTkuMzI4MTI1IDUuNDYwOT  |
  |     |              |           |  M4IDUuNjI4OTA2LjIyMjY1NiAx  |
  |     |              |           |  Mi4xMjUuMjE4NzVjNi41MTE3MT  |
  |     |              |           |  kgMCAxMS43OTY4NzUgNS4yODkw  |
  |     |              |           |  NjIgMTEuNzk2ODc1IDExLjgwMD  |
  |     |              |           |  c4MXptLTkuNzYxNzE5IDIuNTgy  |
  |     |              |           |  MDMxYzAtMS40NTMxMjQgMC0yLj  |
  |     |              |           |  kwNjI1LjAwMzkwNi00LjM1OTM3  |
  |     |              |           |  NCAwLS41NTA3ODItLjE5NTMxMi  |
  |     |              |           |  0xLjAxNTYyNi0uNjAxNTYyLTEu  |
  |     |              |           |  Mzc4OTA3LS42MzY3MTktLjU3MD  |
  |     |              |           |  MxMi0xLjM4MjgxMi0uNjkxNDA2  |
  |     |              |           |  LTIuMTc5Njg4LS40MTAxNTYtLj  |
  |     |              |           |  c2MTcxOC4yNjk1MzEtMS4yNzM0  |
  |     |              |           |  MzcgMS4wMDM5MDYtMS4yNjk1Mz  |
  |     |              |           |  EgMS44MzIwMzEuMDE1NjI1IDIu  |
  |     |              |           |  ODk0NTMyLjAwNzgxMyA1Ljc4OT  |
  |     |              |           |  A2My4wMDM5MDcgOC42ODc1IDAg  |
  |     |              |           |  MS42MzY3MTkgMS40MDIzNDMgMS  |
  |     |              |           |  45MTAxNTYgMi4wNzQyMTggMS45  |
  |     |              |           |  MTAxNTYgMS4wMDc4MTMtLjAwMz  |
  |     |              |           |  kwNiAxLjk5NjA5NC0uODE2NDA2  |
  |     |              |           |  IDEuOTc2NTYzLTEuOTg0Mzc0LS  |
  |     |              |           |  4wMjczNDQtMS40Mjk2ODgtLjAw  |
  |     |              |           |  NzgxMy0yLjg2MzI4Mi0uMDA3OD  |
  |     |              |           |  EzLTQuMjk2ODc2em0uMTQwNjI1  |
  |     |              |           |  LTkuMjkyOTY4Yy0uMDAzOTA2LT  |
  |     |              |           |  EuMjA3MDMyLS45NjQ4NDMtMi4x  |
  |     |              |           |  Njc5NjktMi4xNzU3ODEtMi4xNj  |
  |     |              |           |  c5NjktMS4yMTQ4NDQgMC0yLjE3  |
  |     |              |           |  OTY4OC45Njg3NS0yLjE3OTY4OC  |
  |     |              |           |  AyLjE3OTY4Ny4wMDM5MDcgMS4x  |
  |     |              |           |  OTkyMTkuOTgwNDY5IDIuMTc1Nz  |
  |     |              |           |  gyIDIuMTc5Njg4IDIuMTc1Nzgy  |
  |     |              |           |  IDEuMjEwOTM4IDAgMi4xNzU3OD  |
  |     |              |           |  EtLjk3MjY1NiAyLjE3NTc4MS0y  |
  |     |              |           |  LjE4NzV6bTAgMCIgZmlsbD0iI2  |
  |     |              |           |          ZmZiIvPgo8L3N2Zz4K  |

<?xml version="1.0" encoding="UTF-8"?>
<svg width="80" height="20" xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink">
    <linearGradient id="b" x2="0" y2="100%">
        <stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
        <stop offset="1" stop-opacity=".1"/>
    </linearGradient>
    <mask id="badge">
        <rect width="80" height="20" rx="3" fill="#FFFFFF"/>
    </mask>
    <g mask="url(#badge)">
        <path fill="#FFA500" d="M0 0h80v20h-80z"/>
        <path fill="url(#b)" d="M0 0h80v20H0z"/>
    </g>
    <image x="3" y="3" width="14" height="14" viewBox="0 0 14 14"
    xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcm
    cvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+CiAgPHBhdGggZD0iTTIzLjkyMTg3NSAxMi
    4wMTk1MzFjLS4wMDc4MTMgNi40OTIxODgtNS4yNDYwOTQgMTEuODAwNzgxLTExLjgwNDY4NyAxMS
    43OTY4NzVDNS42MDE1NjIgMjMuODEyNS4zMjQyMTkgMTguNTU0Njg4LjMyNDIxOSAxMi4wMTE3MT
    kuMzI4MTI1IDUuNDYwOTM4IDUuNjI4OTA2LjIyMjY1NiAxMi4xMjUuMjE4NzVjNi41MTE3MTkgMC
    AxMS43OTY4NzUgNS4yODkwNjIgMTEuNzk2ODc1IDExLjgwMDc4MXptLTkuNzYxNzE5IDIuNTgyMD
    MxYzAtMS40NTMxMjQgMC0yLjkwNjI1LjAwMzkwNi00LjM1OTM3NCAwLS41NTA3ODItLjE5NTMxMi
    0xLjAxNTYyNi0uNjAxNTYyLTEuMzc4OTA3LS42MzY3MTktLjU3MDMxMi0xLjM4MjgxMi0uNjkxND
    A2LTIuMTc5Njg4LS40MTAxNTYtLjc2MTcxOC4yNjk1MzEtMS4yNzM0MzcgMS4wMDM5MDYtMS4yNj
    k1MzEgMS44MzIwMzEuMDE1NjI1IDIuODk0NTMyLjAwNzgxMyA1Ljc4OTA2My4wMDM5MDcgOC42OD
    c1IDAgMS42MzY3MTkgMS40MDIzNDMgMS45MTAxNTYgMi4wNzQyMTggMS45MTAxNTYgMS4wMDc4MT
    MtLjAwMzkwNiAxLjk5NjA5NC0uODE2NDA2IDEuOTc2NTYzLTEuOTg0Mzc0LS4wMjczNDQtMS40Mj
    k2ODgtLjAwNzgxMy0yLjg2MzI4Mi0uMDA3ODEzLTQuMjk2ODc2em0uMTQwNjI1LTkuMjkyOTY4Yy
    0uMDAzOTA2LTEuMjA3MDMyLS45NjQ4NDMtMi4xNjc5NjktMi4xNzU3ODEtMi4xNjc5NjktMS4yMT
    Q4NDQgMC0yLjE3OTY4OC45Njg3NS0yLjE3OTY4OCAyLjE3OTY4Ny4wMDM5MDcgMS4xOTkyMTkuOT
    gwNDY5IDIuMTc1NzgyIDIuMTc5Njg4IDIuMTc1NzgyIDEuMjEwOTM4IDAgMi4xNzU3ODEtLjk3Mj
    Y1NiAyLjE3NTc4MS0yLjE4NzV6bTAgMCIgZmlsbD0iI2ZmZiIvPgo8L3N2Zz4K" />
    <g fill="#FFFFFF" text-anchor="middle" font-family="DejaVu
    Sans,Verdana,Geneva,sans-serif" font-size="11">
        <text x="51" y="15" fill="#010101" fill-opacity=".3">label text</text>
        <text x="50" y="14">label text</text>
    </g>
</svg>

```

## Multi-Section Usage

### Input

```
badgingbadger :default:info "Some Information:blue" value:with:colons:red:
```

### Output

![Badge Standard](../images/badge_multisection.svg)

```
<?xml version="1.0" encoding="UTF-8"?>
<svg width="230" height="20" xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink">
    <linearGradient id="b" x2="0" y2="100%">
        <stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
        <stop offset="1" stop-opacity=".1"/>
    </linearGradient>
    <mask id="badge">
        <rect width="230" height="20" rx="3" fill="#FFFFFF"/>
    </mask>
    <g mask="url(#badge)">
        <path fill="#555555" d="M0 0h20v20h-20z"/>
        <path fill="#0000FF" d="M20 0h106v20h-106z"/>
        <path fill="#FF0000" d="M126 0h104v20h-104z"/>
        <path fill="url(#b)" d="M0 0h230v20H0z"/>
    </g>
    <image x="3" y="3" width="14" height="14" viewBox="0 0 14 14"
    xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcm
    cvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+CiAgPHBhdGggZD0iTTIzLjkyMTg3NSAxMi
    4wMTk1MzFjLS4wMDc4MTMgNi40OTIxODgtNS4yNDYwOTQgMTEuODAwNzgxLTExLjgwNDY4NyAxMS
    43OTY4NzVDNS42MDE1NjIgMjMuODEyNS4zMjQyMTkgMTguNTU0Njg4LjMyNDIxOSAxMi4wMTE3MT
    kuMzI4MTI1IDUuNDYwOTM4IDUuNjI4OTA2LjIyMjY1NiAxMi4xMjUuMjE4NzVjNi41MTE3MTkgMC
    AxMS43OTY4NzUgNS4yODkwNjIgMTEuNzk2ODc1IDExLjgwMDc4MXptLTkuNzYxNzE5IDIuNTgyMD
    MxYzAtMS40NTMxMjQgMC0yLjkwNjI1LjAwMzkwNi00LjM1OTM3NCAwLS41NTA3ODItLjE5NTMxMi
    0xLjAxNTYyNi0uNjAxNTYyLTEuMzc4OTA3LS42MzY3MTktLjU3MDMxMi0xLjM4MjgxMi0uNjkxND
    A2LTIuMTc5Njg4LS40MTAxNTYtLjc2MTcxOC4yNjk1MzEtMS4yNzM0MzcgMS4wMDM5MDYtMS4yNj
    k1MzEgMS44MzIwMzEuMDE1NjI1IDIuODk0NTMyLjAwNzgxMyA1Ljc4OTA2My4wMDM5MDcgOC42OD
    c1IDAgMS42MzY3MTkgMS40MDIzNDMgMS45MTAxNTYgMi4wNzQyMTggMS45MTAxNTYgMS4wMDc4MT
    MtLjAwMzkwNiAxLjk5NjA5NC0uODE2NDA2IDEuOTc2NTYzLTEuOTg0Mzc0LS4wMjczNDQtMS40Mj
    k2ODgtLjAwNzgxMy0yLjg2MzI4Mi0uMDA3ODEzLTQuMjk2ODc2em0uMTQwNjI1LTkuMjkyOTY4Yy
    0uMDAzOTA2LTEuMjA3MDMyLS45NjQ4NDMtMi4xNjc5NjktMi4xNzU3ODEtMi4xNjc5NjktMS4yMT
    Q4NDQgMC0yLjE3OTY4OC45Njg3NS0yLjE3OTY4OCAyLjE3OTY4Ny4wMDM5MDcgMS4xOTkyMTkuOT
    gwNDY5IDIuMTc1NzgyIDIuMTc5Njg4IDIuMTc1NzgyIDEuMjEwOTM4IDAgMi4xNzU3ODEtLjk3Mj
    Y1NiAyLjE3NTc4MS0yLjE4NzV6bTAgMCIgZmlsbD0iI2ZmZiIvPgo8L3N2Zz4K" />
    <g fill="#FFFFFF" text-anchor="middle" font-family="DejaVu
    Sans,Verdana,Geneva,sans-serif" font-size="11">
        <text x="21" y="15" fill="#010101" fill-opacity=".3"></text>
        <text x="20" y="14"></text>
    </g>
    <g fill="#FFFFFF" text-anchor="middle" font-family="DejaVu
    Sans,Verdana,Geneva,sans-serif" font-size="11">
        <text x="74" y="15" fill="#010101" fill-opacity=".3">Some
        Information</text>
        <text x="73" y="14">Some Information</text>
    </g>
    <g fill="#FFFFFF" text-anchor="middle" font-family="DejaVu
    Sans,Verdana,Geneva,sans-serif" font-size="11">
        <text x="179" y="15" fill="#010101"
        fill-opacity=".3">value:with:colons</text>
        <text x="178" y="14">value:with:colons</text>
    </g>
</svg>

```
