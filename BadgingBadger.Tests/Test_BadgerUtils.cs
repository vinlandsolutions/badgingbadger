﻿using System.Drawing;
using CommonCore.Badging;
using FluentAssertions;
using Xunit;

namespace BadgingBadger.Tests
{
    public class Test_BadgerUtils
    {
        [Theory]
        [InlineData(null, true, true)]
        [InlineData("", true, true)]
        // Test http/https/file:// uris
        [InlineData("http://commoncorelibs.gitlab.io/commoncore-badging/badge/license.svg", true)]
        [InlineData("https://commoncorelibs.gitlab.io/commoncore-badging/badge/license.svg", true)]
        [InlineData("https://fake/not/real/image.svg", false)]
        //[InlineData("file://./license.svg", true)]
        //[InlineData("file://fake.svg", false)]
        // Test .svg paths
        //[InlineData("license.svg", true)]// Removed for now because it breaks the CICD
        [InlineData("fake.svg", false)]
        // Test keys
        [InlineData("windows", true)]
        [InlineData("notakey", false)]
        public void TryParseIcon(string input, bool expectedResult, bool expectIconNull = false)
        {
            BadgerUtils.TryParseIcon(input, out string icon).Should().Be(expectedResult);
            (icon is null).Should().Be((!expectedResult || expectIconNull ? true : false));
        }

        [Theory]
        [InlineData(null, true, true)]
        [InlineData("", true, true)]
        // Test HEXs
        [InlineData("#000", true)]
        [InlineData("#000000", true)]
        [InlineData("#", false)]
        [InlineData("#00", false)]
        [InlineData("#00000", false)]
        // Test RGBs
        [InlineData("0,0,0", true)]
        [InlineData("0,0,0,0", true)]
        [InlineData(" 0 , 0 , 0 ", true)]
        [InlineData("-1,-1,-1,-1", true)]
        [InlineData("256,256,256,256", true)]
        [InlineData("0,0", false)]
        [InlineData("0,0,0,0,0", false)]
        // Test keys
        [InlineData("default", true, true)]
        [InlineData("red", true)]
        [InlineData("ReD", true)]
        [InlineData("notakey", false)]
        public void TryParseColor(string input, bool expectedResult, bool expectDefaultColor = false)
        {
            BadgerUtils.TryParseColor(input, out Color color).Should().Be(expectedResult);
            if (!expectedResult || expectDefaultColor) { color.Should().Be(BadgeColors.Default); }
        }

        [Theory]
        [InlineData(null, null, null, null)]
        [InlineData("", null, null, null)]
        [InlineData("label", "label", null, null)]
        [InlineData("label:color", "label", "color", null)]
        [InlineData("label:color:icon", "label", "color", "icon")]
        [InlineData("label::", "label", null, null)]
        [InlineData(":color:", null, "color", null)]
        [InlineData("::icon", null, null, "icon")]
        [InlineData("label::icon", "label", null, "icon")]
        [InlineData(":color:icon", null, "color", "icon")]
        [InlineData("label:color:", "label", "color", null)]
        // Test label containing colons.
        [InlineData("a:b:color:icon", "a:b", "color", "icon")]
        // Shows silent error, b is considered color and color is considered the icon.
        [InlineData("a:b:color", "a", "b", "color")]
        // Shows silent error, b is considered color and color is considered null.
        [InlineData("a:b", "a", "b", null)]
        public void SplitSectionInput(string input, string expectedLabel, string expectedColor, string expectedIcon)
            => BadgerUtils.SplitSectionInput(input).Should().Be((expectedLabel, expectedColor, expectedIcon));

        [Theory]
        [InlineData(null, 0, null)]// subject null
        [InlineData(new[] { "" }, 0, null)]// element is empty
        [InlineData(new[] { "a", "b" }, -1, null)]// index < 0
        [InlineData(new[] { "a", "b" }, 2, null)]// index >= length
        [InlineData(new[] { "a", "b" }, 0, "a")]
        [InlineData(new[] { "a", "b" }, 1, "b")]
        public void SafeGet(string[] subject, int index, string expected)
            => BadgerUtils.SafeGet(subject, index).Should().Be(expected);

        [Theory]
        [InlineData("text", "fake", "fake", false, false)]
        [InlineData("text", "fake", "windows", false, true)]
        [InlineData("text", "red", "fake", true, false)]
        [InlineData("text", "red", "windows", true, true)]
        public void TryParseSection(string labelInput, string colorInput, string iconInput,
            bool expectedColorValid, bool expectedIconValid, bool expectSectionNull = false)
        {
            BadgerUtils.TryParseSection(labelInput, colorInput, iconInput, out BadgeSection section)
                .Should().Be((expectedColorValid, expectedIconValid));
            if (!expectedColorValid || !expectedIconValid || expectSectionNull)
            { section.Should().BeNull(); }
        }

        [Theory]
        // Don't pass 0 as a subject, it will fail because of the test code.
        // Don't pass negative numbers, the code tests -subject.
        [InlineData(1, 1, 9)]
        [InlineData(2, 10, 99)]
        [InlineData(3, 100, 999)]
        [InlineData(4, 1_000, 9_999)]
        [InlineData(5, 10_000, 99_999)]
        [InlineData(6, 100_000, 999_999)]
        [InlineData(7, 1_000_000, 9_999_999)]
        [InlineData(8, 10_000_000, 99_999_999)]
        [InlineData(9, 100_000_000, 999_999_999)]
        [InlineData(10, 1_000_000_000, 2_147_483_647)]
        public void GetLength(int expected, params int[] subjects)
        {
            foreach (var subject in subjects)
            {
                BadgerUtils.GetLength(subject, false).Should().Be(expected);
                BadgerUtils.GetLength(-subject, false).Should().Be(expected + 1);
            }
        }

        [Theory]
        // Don't pass 0 as a subject, it will fail because of the test code.
        // Don't pass negative numbers, the code tests -subject.
        [InlineData(1, 1, 9)]
        [InlineData(2, 10, 99)]
        [InlineData(3, 100, 999)]
        [InlineData(5, 1_000, 9_999)]
        [InlineData(6, 10_000, 99_999)]
        [InlineData(7, 100_000, 999_999)]
        [InlineData(9, 1_000_000, 9_999_999)]
        [InlineData(10, 10_000_000, 99_999_999)]
        [InlineData(11, 100_000_000, 999_999_999)]
        [InlineData(13, 1_000_000_000, 2_147_483_647)]
        public void GetLength_IncludeCommas(int expected, params int[] subjects)
        {
            foreach (var subject in subjects)
            {
                BadgerUtils.GetLength(subject, true).Should().Be(expected);
                BadgerUtils.GetLength(-subject, true).Should().Be(expected + 1);
            }
        }
    }
}
