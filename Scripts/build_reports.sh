#!/usr/bin/bash

echo "======================================================================"
echo "BUILDING REPORTS ${title}"
echo "----------------------------------------------------------------------"
echo "Verify Perl Installed..."
perl -v
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING cloc"
echo "----------------------------------------------------------------------"
apt-get update -y -qq && apt-get install -y -qq cloc
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING envsubst"
echo "----------------------------------------------------------------------"
apt-get update -y -qq && apt-get install -y -qq gettext-base
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING dotnet-reportgenerator-globaltool"
echo "----------------------------------------------------------------------"
dotnet tool install dotnet-reportgenerator-globaltool --tool-path tools
echo "======================================================================"

echo "======================================================================"
echo "MAKING DIRECTORIES"
echo "----------------------------------------------------------------------"
mkdir -p --verbose public/reports
echo "======================================================================"

echo "======================================================================"
echo "RUNNING REPORTGENERATOR"
echo "----------------------------------------------------------------------"
tools/reportgenerator "-reports:public/stats/coverage.opencover.xml" "-targetdir:public/reports" "-reportTypes:HTMLInline;HTMLSummary;HTMLChart;MHTML"
echo "======================================================================"
echo "REPLACE REFERENCES"
echo "----------------------------------------------------------------------"
sed -i 's/\.htm/\.html/g' public/reports/*.htm
echo "======================================================================"
echo "RENAME FILES"
echo "----------------------------------------------------------------------"
for f in public/reports/*.htm; do mv --verbose "$f" "${f%htm}html"; done
echo "======================================================================"

echo "======================================================================"
echo "RUNNING CLOC"
echo "----------------------------------------------------------------------"
cloc BadgingBadger BadgingEditor --file-encoding=UTF-8 --skip-archive='(7z|zip|tar(.(gz|Z|bz2|xz|7z))?)' --exclude-dir=obj,bin --exclude-ext=Designer.cs --sum-one --hide-rate --quiet --out=public/reports/cloc_langs.md --md
sed -i 1d public/reports/cloc_langs.md
sed -i 1d public/reports/cloc_langs.md
sed -i 1d public/reports/cloc_langs.md
cat public/reports/cloc_langs.md
echo "----------------------------------------------------------------------"
cloc BadgingBadger BadgingEditor --include-lang=C# --file-encoding=UTF-8 --skip-archive='(7z|zip|tar(.(gz|Z|bz2|xz|7z))?)' --exclude-dir=obj,bin --exclude-ext=Designer.cs --sum-one --by-file --hide-rate --quiet --out=public/reports/cloc_files.md --md
sed -i 1d public/reports/cloc_files.md
sed -i 1d public/reports/cloc_files.md
sed -i 1d public/reports/cloc_files.md
cat public/reports/cloc_files.md
echo "----------------------------------------------------------------------"
cloc_langs=$(cat public/reports/cloc_langs.md) cloc_files=$(cat public/reports/cloc_files.md) envsubst < ${project}.Docs/docs/cloc.md > public/reports/cloc.md
echo "======================================================================"
