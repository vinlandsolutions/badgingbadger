echo "======================================================================"
echo "TESTING PRODUCT ${title}"
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING libgdiplus"
echo "----------------------------------------------------------------------"
apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated libc6-dev libgdiplus libx11-dev
rm -rf /var/lib/apt/lists/*
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING coverlet.console"
echo "----------------------------------------------------------------------"
dotnet tool install coverlet.console --tool-path tools
echo "======================================================================"

echo "======================================================================"
echo "MAKING DIRECTORIES"
echo "----------------------------------------------------------------------"
mkdir -p --verbose public/stats
echo "======================================================================"

echo "======================================================================"
echo "RUNNING TESTS"
echo "----------------------------------------------------------------------"
dotnet test ${project}.Tests/${project}.Tests.csproj -c Release
echo "======================================================================"

echo "======================================================================"
echo "ANALYZE TEST COVERAGE"
echo "----------------------------------------------------------------------"
tools/coverlet "${project}.Tests/bin/Release/netcoreapp3.1/${project}.Tests.dll" --target "dotnet" --targetargs "test ${project}.Tests/${project}.Tests.csproj --no-build -c Release" --output "./public/stats/" -f json -f opencover | tee public/stats/coverage.txt
echo "======================================================================"
