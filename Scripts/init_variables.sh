
echo "======================================================================"
echo "INITIALIZING VARIABLES"
echo "${CI_COMMIT_BRANCH} - ${CI_COMMIT_SHA} - ${CI_COMMIT_TAG}"
echo "----------------------------------------------------------------------"
echo "Verify Bash Installed..."
bash --version
echo "----------------------------------------------------------------------"
project="${CI_PROJECT_TITLE}";
version="$(cat ./version.txt)"
echo "Version: ${version}"
array=($(echo $version | tr '.-' ' '))
echo "Array:   ${array[@]}"
if [ -z "${CI_COMMIT_TAG}" ]
then
    echo "    Bumping to temp dev version."
    if [ ${#array[@]} -eq 3 ]
    then
        echo "        Adding pre.0 section."
        version="${array[0]}.${array[1]}.$((${array[2]}+1))-pre.0"
    else
        echo "        Bumping pre section."
        version="${array[0]}.${array[1]}.${array[2]}-${array[3]}.$((${array[4]}+1))"
    fi
else
    echo "Using current version."
fi;
unset array
title="${toolid}.${version}"
echo "----------------------------------------------------------------------"
echo "Tool ID:   ${toolid}"
echo "Tool Name: ${toolname}"
echo "Company:   ${company}"
echo "Project:   ${project}"
echo "Version:   ${version}"
echo "Title:     ${title}"
echo "======================================================================"
echo "Project Namespace: ${CI_PROJECT_NAMESPACE}"
echo "Project Title:     ${CI_PROJECT_TITLE}"
echo "Project Name:      ${CI_PROJECT_NAME}"
echo "Project Path:      ${CI_PROJECT_PATH}"
echo "Project Dir:       ${CI_PROJECT_DIR}"
echo "Project Url:       ${CI_PROJECT_URL}"
echo "----------------------------------------------------------------------"
echo "Commit Ref Name:   ${CI_COMMIT_REF_NAME}"
echo "Commit Branch:     ${CI_COMMIT_BRANCH}"
echo "Commit SHA:        ${CI_COMMIT_SHA}"
echo "Commit Tag:        ${CI_PROJECT_TAG}"
echo "----------------------------------------------------------------------"
echo "${CI_COMMIT_TITLE}"
echo ""
echo "${CI_COMMIT_DESCRIPTION}"
echo "----------------------------------------------------------------------"
echo "${CI_COMMIT_MESSAGE}"
echo "======================================================================"
