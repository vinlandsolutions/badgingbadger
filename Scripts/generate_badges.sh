#!/usr/bin/bash

echo "======================================================================"
echo "BUILDING BADGES ${title}"
echo "======================================================================"

echo "======================================================================"
echo "INITIALIZING SSH"
echo "----------------------------------------------------------------------"
mkdir -p ~/.ssh && chmod 700 ~/.ssh
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && chmod 644 ~/.ssh/known_hosts
eval $(ssh-agent -s)
ssh-add <(echo "$SSH_PRIVATE_KEY")
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING python-gitlab"
echo "----------------------------------------------------------------------"
pip install python-gitlab
echo "======================================================================"

echo "======================================================================"
echo "MAKING DIRECTORIES"
echo "----------------------------------------------------------------------"
mkdir -p --verbose public/badge/{coverage,issues,tests,version}
echo "======================================================================"

echo "======================================================================"
echo "RUNNING BADGE GEN"
echo "----------------------------------------------------------------------"
python3 Pipeline/gen-badges.py -t $GitlabApiKey -i $CI_PROJECT_ID -c public/stats/coverage.txt
echo "======================================================================"
