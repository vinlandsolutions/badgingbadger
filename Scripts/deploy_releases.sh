#!/usr/bin/bash

newline=$'\n'
echo "======================================================================"
echo "DEPLOYING RELEASES ${title}"
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING jq"
echo "----------------------------------------------------------------------"
#apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated jq
apk update && apk add jq
echo "======================================================================"

echo "======================================================================"
echo "INSTALLING curl"
echo "----------------------------------------------------------------------"
#apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated curl
apk add curl
echo "======================================================================"

echo "======================================================================"
echo "CREATE RELEASE"
echo "----------------------------------------------------------------------"
# mark release and link builds in release message
curl --request POST --header "PRIVATE-TOKEN: ${GitlabApiKey}" --data "description=# v${CI_COMMIT_TAG}${newline}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags/${CI_COMMIT_TAG}/release
echo "======================================================================"

echo "======================================================================"
echo "UPLOAD ARTIFACTS"
echo "----------------------------------------------------------------------"
for filename in "${title}-linux-x64.zip" "${title}-mac-x64.zip" "${title}-win-x64.zip" "${title}.nupkg"
do
echo "======================================================================"
echo "UPLOAD $filename ($filepath)"
echo "----------------------------------------------------------------------"
filepath="public/product/${filename}"
content=$(curl --request POST --header "PRIVATE-TOKEN: ${GitlabApiKey}" --form "file=@${filepath}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/uploads)
echo "$content"
# get the url from the json return
rel_url=$(jq -r '.url' <<<"$content")
echo "======================================================================"
echo "LINK ASSET $filename ($filepath) ==> ${CI_PROJECT_URL}${rel_url}"
echo "----------------------------------------------------------------------"
curl --request POST --header "PRIVATE-TOKEN: ${GitlabApiKey}" --data "name=${filename}" --data "url=${CI_PROJECT_URL}${rel_url}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links
done
echo "======================================================================"
