﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using CommonCore.Badging;

namespace BadgingBadger
{
    public static class BadgerUtils
    {
        public static bool TryParseIcon(string input, out string icon)
        {
            if (input is null || input.Length == 0) { icon = null; return true; }
            else if (input.StartsWith("https://") || input.StartsWith("http://"))// || text.StartsWith("file:///")
            {
                try { icon = new Uri(input).DownloadBase64(); return true; }
                catch { icon = null; return false; }
            }
            else if (input.ToLowerInvariant().EndsWith(".svg"))
            {
                try { icon = new Uri("file:///" + Path.GetFullPath(input)).DownloadBase64(); return true; }
                catch { icon = null; return false; }
            }
            else if (BadgeIcons.Dict.ContainsKey(input.ToLowerInvariant()))
            {
                icon = BadgeIcons.Dict[input];
                return true;
            }
            else
            {
                icon = null;
                return false;
            }
        }

        public static bool TryParseColor(string input, out Color color)
        {
            if (input is null || input.Length == 0)
            {
                color = BadgeColors.Default;
                return true;
            }
            else if (input.StartsWith("#"))
            {
                if (input.Length == 4 || input.Length == 7)// #FFF or #FFFFFF
                {
                    color = ColorTranslator.FromHtml(input);
                    return true;
                }
                else
                {
                    color = BadgeColors.Default;
                    return false;
                }
            }
            else if (input.Contains(","))
            {
                var values = input.Split(',');
                if (values.Length < 3 || values.Length > 4
                    || !int.TryParse(values[0], out int r)
                    || !int.TryParse(values[1], out int g)
                    || !int.TryParse(values[2], out int b))
                {
                    color = BadgeColors.Default;
                    return false;
                }

                if (values.Length == 3 || !int.TryParse(values[3], out int a))
                { a = 255; }

                r = (r < 0 ? 0 : r > 255 ? 255 : r);
                g = (g < 0 ? 0 : g > 255 ? 255 : g);
                b = (b < 0 ? 0 : b > 255 ? 255 : b);
                a = (a < 0 ? 0 : a > 255 ? 255 : a);
                color = Color.FromArgb(a, r, g, b);
                return true;
            }
            else if (BadgeColors.Dict.ContainsKey(input.ToLowerInvariant()))
            {
                color = BadgeColors.Dict[input];
                return true;
            }
            else
            {
                color = BadgeColors.Default;
                return false;
            }
        }

        public static bool TryParseSection(int index, string input, IList<string> errors, out BadgeSection section)
        {
            var (labelInput, colorInput, iconInput) = BadgerUtils.SplitSectionInput(input);
            var (colorValid, iconValid) = BadgerUtils.TryParseSection(labelInput, colorInput, iconInput, out section);
            if (!colorValid) { errors.Add($"Section {index} '{input}': Invalid color input: '{colorInput}'"); }
            if (!iconValid) { errors.Add($"Section {index} '{input}': Invalid icon input: '{iconInput}'"); }
            return errors.Count == 0;
        }

        public static (string labelInput, string colorInput, string iconInput) SplitSectionInput(string input)
        {
            if (input == null || input.Length == 0) { return (null, null, null); }
            var split = input.Split(':', StringSplitOptions.None);
            return split.Length < 4
                ? (SafeGet(split, 0), SafeGet(split, 1), SafeGet(split, 2))
                : (string.Join(':', split[0..^2]), split[^2], split[^1]);
        }

        public static (bool colorValid, bool iconValid) TryParseSection(string labelInput, string colorInput, string iconInput, out BadgeSection section)
        {
            var colorValid = BadgerUtils.TryParseColor(colorInput, out Color color);
            var iconValid = BadgerUtils.TryParseIcon(iconInput, out string icon);
            section = (colorValid && iconValid ? new BadgeSection(labelInput, color, icon) : null);
            return (colorValid, iconValid);
        }

        /// <summary>
        /// Returns a <see cref="string"/> element from the specified <paramref name="array"/> if the
        /// specified <paramref name="index"/> is valid; otherwise <c>default</c> if
        /// the <paramref name="array"/> is <c>null</c>;
        /// the <paramref name="array"/> contains no elements;
        /// the <paramref name="index"/> is less than <c>0</c>;
        /// or the <paramref name="index"/> is greater than or equal to the <paramref name="array"/> length.
        /// </summary>
        /// <param name="array">The <see cref="string"/> array to safely get an element from.</param>
        /// <param name="index">The index of the element to safely get.</param>
        /// <returns>
        /// A <see cref="string"/> element from the specified <paramref name="array"/> if the
        /// specified <paramref name="index"/> is valid; otherwise <c>default</c>.
        /// </returns>
        public static string SafeGet(string[] array, int index)
            => (array == null || index < 0 || index >= array.Length ? default : array[index].Length == 0 ? default : array[index]);

        public static int GetLength(int number, bool includeCommas = false)
        {
            bool negative = number < 0;
            if (negative) { number = -number; }

            int length;
            if (number < 10) { length = 1; }
            else if (number < 100) { length = 2; }
            else if (number < 1_000) { length = 3; }
            else if (number < 10_000) { length = 4; }
            else if (number < 100_000) { length = 5; }
            else if (number < 1_000_000) { length = 6; }
            else if (number < 10_000_000) { length = 7; }
            else if (number < 100_000_000) { length = 8; }
            else if (number < 1_000_000_000) { length = 9; }
            else { length = 10; }

            if (includeCommas) { length += (length - 1) / 3; }
            if (negative) { length += 1; }
            return length;
        }
    }
}
