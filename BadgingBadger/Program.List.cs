﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.CommandLine.Parsing;
using BadgingBadger.Screens;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Rendering.Screens;

namespace BadgingBadger
{
    static partial class Program
    {
        //public class ColumnSelectorTypeConverter : TypeConverter
        //{
        //    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        //        => sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);

        //    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        //        => value is string str ? new ColumnSelector(str) : base.ConvertFrom(context, culture, value);
        //}

        //[TypeConverter(typeof(ColumnSelectorTypeConverter))]
        //public class ColumnSelector
        //{
        //    public ColumnSelector(string input)
        //    {
        //    }
        //}

        /// <summary>
        /// List the various built-in badge styles.
        /// </summary>
        [CommandReference(nameof(Colors))]
        [CommandReference(nameof(Fonts))]
        [CommandReference(nameof(Icons))]
        static void List(ParseResult result)
            => HelpScreen.Instance.Show(result);

        public enum EColorsStyle { Table = 0, Count, Index, Key, Hex, Rgb }

        /// <summary>
        /// List the built-in badge colors.
        /// </summary>
        static void Colors(EColorsStyle style = default)
        {
            var screen = style switch
            {
                EColorsStyle.Table => ListColorsScreen.Instance,
                EColorsStyle.Count => new Screen().Add(ListColorsScreen.Instance.Count.ToString("#,##0")),
                EColorsStyle.Index => new Screen().Add(ListColorsScreen.Instance.IndexTexts),
                EColorsStyle.Key => new Screen().Add(ListColorsScreen.Instance.KeyTexts),
                EColorsStyle.Hex => new Screen().Add(ListColorsScreen.Instance.HexTexts),
                EColorsStyle.Rgb => new Screen().Add(ListColorsScreen.Instance.RgbTexts),
                _ => throw new NotSupportedException(),
            };
            screen.Render();
        }

        public enum EFontsStyle { Table = 0, Count, Index, Key }

        /// <summary>
        /// List the built-in badge fonts.
        /// </summary>
        static void Fonts(EFontsStyle style = default)
        {
            var screen = style switch
            {
                _ => ListFontsScreen.Instance
                //EFontsStyle.Table => ListFontsScreen.Instance,
                //EFontsStyle.Count => new Screen().Add(ListFontsScreen.Instance.Count.ToString("#,##0")),
                //EFontsStyle.Index => new Screen().Add(ListFontsScreen.Instance.IndexTexts),
                //EFontsStyle.Key => new Screen().Add(ListFontsScreen.Instance.KeyTexts),
                //_ => throw new NotSupportedException(),
            };
            screen.Render();
        }

        public enum EIconsStyle { Table = 0, Count, Total, Index, Key, Size, Content }

        /// <summary>
        /// List the built-in badge icons.
        /// </summary>
        static void Icons(EIconsStyle style = default)
        {
            var screen = style switch
            {
                EIconsStyle.Table => ListIconsScreen.Instance,
                EIconsStyle.Count => new Screen().Add(ListIconsScreen.Instance.Count.ToString("#,##0")),
                EIconsStyle.Total => new Screen().Add(ListIconsScreen.Instance.TotalSize.ToString("#,##0")),
                EIconsStyle.Index => new Screen().Add(ListIconsScreen.Instance.IndexTexts),
                EIconsStyle.Key => new Screen().Add(ListIconsScreen.Instance.KeyTexts),
                EIconsStyle.Size => new Screen().Add(ListIconsScreen.Instance.SizeTexts),
                EIconsStyle.Content => new Screen().Add(ListIconsScreen.Instance.ContentTexts),
                _ => throw new NotSupportedException(),
            };
            screen.Render();
        }
    }
}
