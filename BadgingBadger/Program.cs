﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.CommandLine.Parsing;
using System.IO;
using CommonCore.Badging;
using CommonCore.CommandLine;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering.Screens;
using CommonCore.CommandLine.Suggestions;

namespace BadgingBadger
{
    [EnableDefaultExceptionReporting]
    [EnableDefaultErrorReporting]
    [EnableDefaultGlobalOptions(true)]
    [AppMITLicense()]
    static partial class Program
    {
        static Program()
        {
            var source = new SuggestSource(_ => new[] { "file", "-" });
            Services.Suggestions.AddOrUpdate(typeof(FileInfo), source, (_0, _1) => source);
            Services.Suggestions.TryAdd(typeof(BadgeSection), new SuggestSource(_ => new[] { "label:color:icon" }));
            //BadgeColors.Dict.Add("bgreen", ColorTranslator.FromHtml("#33CC11"));
            //BadgeColors.Dict.Add("bblue", ColorTranslator.FromHtml("#0088CC"));
            //BadgeColors.Dict.Add("byellow", ColorTranslator.FromHtml("#DDBB11"));
            //BadgeColors.Dict.Add("borange", ColorTranslator.FromHtml("#FF7733"));
            //BadgeColors.Dict.Add("bred", ColorTranslator.FromHtml("#EE4433"));
            //BadgeColors.Dict.Add("blightgray", ColorTranslator.FromHtml("#999999"));
            //BadgeColors.Dict.Add("bdarkgray", ColorTranslator.FromHtml("#555555"));
        }

        /// <summary>
        /// Build svg badges.
        /// </summary>
        [CommandEntryPoint]
        [CommandReference(nameof(Build), true)]
        [CommandReference(nameof(List))]
        [CommandReference(nameof(Help))]
        [CommandReference(nameof(License))]
        [CommandReference(nameof(Version))]
        static void Main(ParseResult result) => HelpScreen.Instance.Show(result);

        /// <summary>
        /// Print the program help information.
        /// </summary>
        static void Help(ParseResult result) => HelpScreen.Instance.Show(result);

        /// <summary>
        /// Print the program license information.
        /// </summary>
        static void License() => LicenseScreen.Instance.Show();

        /// <summary>
        /// Print the program version information.
        /// </summary>
        static void Version() => VersionScreen.Instance.Show();
    }
}
