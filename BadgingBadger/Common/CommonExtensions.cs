﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BadgingBadger.Common
{
    public static class CommonExtensions
    {
        public static IEnumerable<T> Yield<T>(this T self) { yield return self; }
    }
}
