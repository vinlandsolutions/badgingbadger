﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonCore.Badging;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Rendering.Views;

namespace BadgingBadger.Screens
{
    public class ListIconsScreen : Screen
    {
        public static ListIconsScreen Instance { get; set; } = new ListIconsScreen();

        public ListIconsScreen()
        {
            this.AddHeader()
                .AddCopyright()
                .AddLicenseSummary()
                .Add()
                .Add($"Count: {this.Count} ({this.TotalSize:#,##0 bytes})");

            if (this.Count == 0) { this.Add("NO ICONS FOUND"); }
            else
            {
                var table = new TableView<(int Index, string Key, string Value, int Size)>()
                { Prefix = "| ", Delimiter = " | ", Suffix = " |" };

                table.Items = this.Dict.Select((pair, index) => (index, pair.Key, pair.Value, Encoding.UTF8.GetByteCount(pair.Value))).ToList();
                table.AddColumn(d => d.Index, "Index", new ColumnDefinition(EAlignment.Right));
                table.AddColumn(d => d.Key, "Key", new ColumnDefinition(EAlignment.Right));
                table.AddColumn(d => d.Size.ToString("#,##0"), "Size (Bytes)", new ColumnDefinition(EAlignment.Right));
                table.AddColumn(d => d.Value, "Content");
                this.Add(table)
                    .Add();
            }
        }

        public IReadOnlyDictionary<string, string> Dict { get; } = BadgeIcons.Dict;

        public int Count => this.Dict.Count;

        public int TotalSize => this.Dict.Sum(pair => Encoding.UTF8.GetByteCount(pair.Value));

        public IEnumerable<string> IndexTexts => Enumerable.Range(0, this.Count).Select(i => i.ToString("#,##0"));

        public IEnumerable<string> KeyTexts => this.Dict.Keys;

        public IEnumerable<string> SizeTexts => this.Dict.Values.Select(value => Encoding.UTF8.GetByteCount(value).ToString("#,##0"));

        public IEnumerable<string> ContentTexts => this.Dict.Values;
    }
}
