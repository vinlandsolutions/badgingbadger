﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using CommonCore.CommandLine.Rendering;

namespace BadgingBadger.Screens
{
    public class ListFontsScreen : Screen
    {
        public static ListFontsScreen Instance { get; set; } = new ListFontsScreen();

        public ListFontsScreen()
        {
            this.AddHeader()
                .AddCopyright()
                .AddLicenseSummary()
                .Add()
                .Add("... coming soon ...")
                .Add()
                .Render();
        }
    }
}
