﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.Linq;
using CommonCore.Badging;
using CommonCore.CommandLine.Rendering;
using CommonCore.CommandLine.Rendering.Views;

namespace BadgingBadger.Screens
{
    public class ListColorsScreen : Screen
    {
        public static ListColorsScreen Instance { get; set; } = new ListColorsScreen();

        public ListColorsScreen(bool verbose = false)
        {
            this.AddHeader()
                .AddCopyright()
                .AddLicenseSummary()
                .Add()
                .Add($"Count: {this.Count}");

            if (this.Count == 0) { this.Add("NO COLORS FOUND"); }
            else
            {
                var table = new TableView<(int Index, string Key, System.Drawing.Color Value)>()
                { Prefix = "| ", Delimiter = " | ", Suffix = " |" };

                table.Items = this.Dict.Select((pair, index) => (index, pair.Key, pair.Value)).ToArray();
                table.AddColumn(d => d.Index, "Index", new ColumnDefinition(EAlignment.Right));
                table.AddColumn(d => d.Key, "Key", new ColumnDefinition(EAlignment.Right));
                table.AddColumn(d => d.Value.ToHex(), "HEX", new ColumnDefinition());
                table.AddColumn(d => d.Value.ToRgb(), "RGB", new ColumnDefinition(EAlignment.Right));

                this.Add(table).Add();
            }
        }

        public IReadOnlyDictionary<string, System.Drawing.Color> Dict { get; } = BadgeColors.Dict;

        public int Count => this.Dict.Count;

        public IEnumerable<string> IndexTexts => Enumerable.Range(0, this.Count).Select(i => i.ToString("#,##0"));

        public IEnumerable<string> KeyTexts => this.Dict.Keys;

        public IEnumerable<string> HexTexts => this.Dict.Values.Select(c => c.ToHex());

        public IEnumerable<string> RgbTexts => this.Dict.Values.Select(c => c.ToRgb());
    }
}
