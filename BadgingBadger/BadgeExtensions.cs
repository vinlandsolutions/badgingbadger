﻿//#region Copyright (c) 2020 Jay Jeckel
//// Copyright (c) 2020 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System.Collections.Generic;
//using CommonCore.Badging;

//namespace BadgingBadger
//{
//    public static class BadgeExtensions
//    {
//        public static Badge FromConsoleInput(this Badge self, string section, IEnumerable<string> sections = null)
//        {
//            {
//                var (labelInput, colorInput, iconInput) = BadgerUtils.SplitSectionInput(section);
//                BadgerUtils.TryParseSection(labelInput, colorInput, iconInput, out BadgeSection sec);
//                if (sec != null) { self.Sections.Add(sec); }
//            }
//            if (sections != null)
//            {
//                foreach (var input in sections)
//                {
//                    var (labelInput, colorInput, iconInput) = BadgerUtils.SplitSectionInput(input);
//                    BadgerUtils.TryParseSection(labelInput, colorInput, iconInput, out BadgeSection sec);
//                    if (sec != null) { self.Sections.Add(sec); }
//                }
//            }
//            return self;
//        }
//    }
//}
