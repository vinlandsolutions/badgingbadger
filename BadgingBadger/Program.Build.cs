﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.CommandLine.Parsing;
using System.IO;
using System.Linq;
using CommonCore.Badging;
using CommonCore.CommandLine.AutoBuild.Attributes;
using CommonCore.CommandLine.Rendering;

namespace BadgingBadger
{
    static partial class Program
    {
        /// <summary>
        /// Build a single badge from one or more sections.
        /// </summary>
        /// <param name="sections">Space-separated list of colon-delimited section values. 'label:color:icon'</param>
        /// <param name="format">Select format type used to write the badge.</param>
        /// <param name="output">Write the badge output to the given path instead of stdout.</param>
        /// <param name="verbose">Print additional information.</param>
        [ValidatorReference(nameof(ValidateOptionOutput))]
        static void Build(
            [Argument, Parse(nameof(ParseSections))] BadgeSection[] sections,
            EOutputFormat format = EOutputFormat.Svg,
            [Parse(nameof(ParseFileInfo))] FileInfo output = null,
            bool verbose = false)
        {
            Badge badge = new Badge(sections);
            var screen = new Screen();

            if (verbose)
            {
                bool empty = badge.Sections.Count == 0;
                screen
                    .AddHeader()
                    .AddCopyright()
                    .AddLicenseSummary()
                    .Add()
                    .Add("SETTING(S):")
                        .Add(1,
                            $"Format: {format}",
                            $"Output: {(output is null ? "<stdout>" : output.FullName)}",
                            $"Verbose: {verbose}")
                        .Add()
                    .Add("SECTION(S):")
                        .Add(empty, 1, "NO SECTIONS FOUND")
                        .Add(!empty, 1, $"Count: {badge.Sections.Count}")
                        .AddTable<(int Index, BadgeSection Section), string>(!empty, badge.Sections.Select((s, i) => (i, s)),
                            (null, d => "  " + d.Index),
                            ("Color", d => d.Section.Color.ToHex()),
                            ("Text", d => d.Section.Text),
                            ("Icon", d => d.Section.Icon?.Substring(0, Math.Min(20, d.Section.Icon.Length)) ?? "<null>"))
                        .Add();
            }

            string svg = badge.ToString(format == EOutputFormat.Svg ? EBadgeFormat.Svg : EBadgeFormat.Base64);
            screen.Add(verbose || output is null, svg.Replace("\r\n", "\n").Split('\n'));

            if (!(output is null))
            {
                var path = Path.GetFullPath(output.FullName);
                screen.Add(verbose, "SECTION(S):")
                    .Add(verbose, 1, "Writing badge to:", path)

                    //.WriteAllText(path, svg)
                    .Add($"DEBUGGING: File not written because the line is commented out.",
                         $"DEBUGGING: {path}")

                    .Add(verbose, 1, "Badge written.")
                    .Add();
            }

            screen.Render();
        }

        private static BadgeSection[] ParseSections(ArgumentResult result)
        {
            var sections = new List<BadgeSection>();
            var errors = new List<string>();
            var values = result.Tokens.Select(t => t.Value).ToArray();
            for (int index = 0; index < values.Length; index++)
            {
                if (BadgerUtils.TryParseSection(index, values[index], errors, out BadgeSection section))
                { sections.Add(section); }
            }
            if (errors.Count > 0) { result.ErrorMessage = string.Join('\n', errors); }
            return sections.ToArray();
        }

        private static FileInfo ParseFileInfo(ArgumentResult result)
        {
            var value = result.Tokens.Single().Value;
            return value.Length == 1 && value[0] == '-' ? null : new FileInfo(value);
        }

        public static string ValidateOptionOutput(CommandResult result)
        {
            var file = result.ValueForOption<FileInfo>("output");
            if (file is null) { return null; }
            string error = null;
            try { file.OpenRead().Dispose(); }
            catch (ArgumentException)
            { error = $"Output file value is not a valid path."; }
            catch (PathTooLongException)
            { error = $"Output file value length exceeds path limit."; }
            catch (DirectoryNotFoundException)
            { error = $"Output file value contains a non-existent directory."; }
            catch (NotSupportedException)
            { error = $"Output file value is in an invalid format."; }
            catch (UnauthorizedAccessException)
            { error = $"Output file could not be accessed."; }
            catch (FileNotFoundException)
            { error = $"Output file could not be found."; }
            catch (System.Security.SecurityException)
            { error = $"Output file requires elevated permission."; }
            catch (IOException e)
            { error = $"Output file could not be read: {e.Message}"; }
            catch (Exception e)
            { error = $"Output file could not be read: {e.Message}"; }
            //try
            //{
            //    var path = Path.GetFullPath(file.FullName);
            //    var dir = Path.GetDirectoryName(path);
            //    if (!Directory.Exists(dir))
            //    { message = $"Part of the output file path does not exist."; }
            //}
            //catch (SecurityException)
            //{ error = "Caller does not have permission to access the output file path."; }
            //catch (ArgumentException)
            //{ error = "The output file path contains invalid characters or the absolute path could not be resolved."; }
            //catch (NotSupportedException)
            //{ error = "The output file path contains a colon that is not part of a volume identifier."; }
            //catch (PathTooLongException)
            //{ error = "The output file path, file name, or both exceed the system-defined maximum length."; }
            //catch (Exception exception)
            //{ error = $"{exception.GetType().Name}: {exception.Message}"; }

            return error;
        }

        public enum EOutputFormat : int
        {
            //None = 0,
            Svg,
            //Png,
            Base64
        }
    }
}
